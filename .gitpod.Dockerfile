FROM gitpod/workspace-full
                    
USER gitpod

RUN sudo apt-get -q update
RUN sudo apt-get install -y \
	cppcheck \
	lcov \
	meson \
	ccache \
	graphviz \
	gpp \
	shelltestrunner \
    clang++
	
