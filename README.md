
# lox-simple

Implementation of the lox scripting language AST interpreter from the book [Crafting Interpreters](http://www.craftinginterpreters.com) by [Bob Nystrom](https://github.com/munificent). This is just an excercise for me nothing else.

**Progress**: ~~Chapter 12~~ Chapter 7

## Goals

* Reflect the JAVA version from the book ~~as close as possible~~
* Use C++ idoms and styles like...
    * RAII (Resource acquisition is initialization)
    * Const Correctness
    * ~~C++14~~ C++17 where feasible
    * Smart Pointers or maybe not
    * No RTTI :-)
* Unit tests for language features

## Rationale

### Why are you not `using namespace std;`?

* Increased verbosity
* I must not use it in header files.
* Resolving the namespace defeats its purpose.

### Why are the AST classes like `Expr` and `Stmt` named different than in the book?

This is mainly a learning experience. I opted for namespaces instead of classes.

### Why are the AST classes not generated?

I code this along as the book itself evolves. Translating the JAVA into *native*
C++ requires some interations. I am too lazy to maintain another tool.

### Why are you using `shared_ptr` for every maybe-null reference?

~~Just because of lazyness and ease of development while following the book.~~
I am re-working memory management to be more *conservative*.
Main goal is to never leak any memory.

### Why are the expressions and visitor interface not implemented as template?

I found it too ugly. 
The whole pattern is just messy to debug.

### What extensions did you add?

* Support for hashbangs/shebangs e.g. `#!/usr/bin/env lox`
* Commands `quit` or `exit` end for the REPL loop

### Why is there no `ParseError` type?

It is pretty much identical to the `RuntimeError` but used in the context of the parser.
One error type is enough IMO.

## How to build

### Dependencies

* Compiler with C++17 support
* cmake >= 3.1 (build)

### Build

```
git clone https://gitlab.com/aggsol/lox-simple.git
cd lox-simple
mkdir build
cd build
cmake ..
cmake --build .
```

Two binaries are created:

*   `lox` is the interpreter
*   `unittest` runs the unittests

## Licenses

* lox-simple is GPLv3
* [tiny-unit](https://github.com/aggsol/tiny-unit) is Beerware
* [CliArguments.hpp](https://gist.github.com/aggsol/6008239) is Beerware

[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/aggsol/lox-simple) 
