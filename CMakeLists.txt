cmake_minimum_required(VERSION 3.5)
project(lox LANGUAGES CXX VERSION 0.1.0)

# C++ requirements for all targets
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# Create compile_commands.json for cppcheck
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Set build type, default is Debug
# You can also set it on the command line: -DCMAKE_BUILD_TYPE=Release
if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build (Debug or Release)" FORCE)
endif()

include_directories("${PROJECT_BINARY_DIR}")

# Set build information in BuildInfo.hpp file
string(TIMESTAMP BUILD_TIMESTAMP %Y%m%d%H%M%S UTC)
configure_file(
  "${PROJECT_SOURCE_DIR}/src/BuildInfo.hpp.in"
  "${PROJECT_BINARY_DIR}/BuildInfo.hpp"
)

# copy binaries to root build dir
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")

# Common compiler flags
add_library(common INTERFACE)

if(MSVC)
target_compile_options(common
INTERFACE
	/permissive-
)
else()
target_compile_options(common
INTERFACE
	-Wall 
	-Wextra 
	-Wshadow 
	-Wnon-virtual-dtor
	-Wold-style-cast
	-Woverloaded-virtual 
	-Wzero-as-null-pointer-constant
	-pedantic 
	-fPIE 
	-fstack-protector-all 
	-fno-rtti
)

endif()

add_subdirectory(src)
add_subdirectory(test)

message(STATUS "Version = ${PROJECT_VERSION}")
