/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "tiny-unit.hpp"

#include "../src/Logger.hpp"
#include "../src/Scanner.hpp"
#include "../src/TokenType.hpp"

#include <iostream>

namespace test
{
class ScannerTest : public tiny::Unit
{
public:

    ScannerTest()
    : tiny::Unit("ScannerTest")
    {
        tiny::Unit::registerTest(&ScannerTest::hashbang, "hashbang");
        tiny::Unit::registerTest(&ScannerTest::nestedComments, "nestedComments");
        tiny::Unit::registerTest(&ScannerTest::scanNumber, "scanNumber");
        tiny::Unit::registerTest(&ScannerTest::scanString, "scanString");
        tiny::Unit::registerTest(&ScannerTest::testScan, "testScan");
        tiny::Unit::registerTest(&ScannerTest::testComment, "testComment");
    }

    static void hashbang()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner1("#!/usr/bin/env lox\n var i=1;\n", logger);
        auto tokens = scanner1.scanTokens();

        TINY_ASSERT_OK(! scanner1.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 6);

        bodhi::Scanner scanner2("\n\n#!/usr/bin/env lox\n var i=1;\n", logger);
        scanner2.scanTokens();

        TINY_ASSERT_OK(scanner2.hasError());
    }

    static void nestedComments()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner1("/* simple block **/", logger);
        auto tokens = scanner1.scanTokens();

        TINY_ASSERT_OK(! scanner1.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 1);

        bodhi::Scanner scanner2("\n\n/** line1\n line2\n */", logger);
        tokens.clear();
        tokens = scanner2.scanTokens();

        TINY_ASSERT_OK(! scanner2.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 1);

        bodhi::Scanner scanner3("\n\n/** /*line1*/\n /*line2*/\n */", logger);
        tokens.clear();
        tokens = scanner3.scanTokens();

        TINY_ASSERT_OK(! scanner3.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 1);
    }

    static void scanNumber()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner1("12.12", logger);
        auto tokens = scanner1.scanTokens();

        TINY_ASSERT_OK(! scanner1.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 2);
        TINY_ASSERT_EQUAL(tokens[0].type(), bodhi::TokenType::NUMBER);

        TINY_ASSERT_EQUAL(tokens[0].literal(), "12.12");
        TINY_ASSERT_EQUAL(scanner1.m_tokens.size(), 0);
    }

    static void scanString()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner1("var language = \"loxs;sdf g", logger);
        scanner1.scanTokens();

        TINY_ASSERT_OK(scanner1.hasError());

        bodhi::Scanner scanner2("var language = \"lox\";", logger);
        auto tokens = scanner2.scanTokens();

        TINY_ASSERT_OK(tokens.size() == 6);
        TINY_ASSERT_EQUAL(tokens[1].type(), bodhi::TokenType::IDENTIFIER);

        TINY_ASSERT_EQUAL(tokens[3].type(), bodhi::TokenType::STRING);
        TINY_ASSERT_EQUAL(tokens[3].literal(), "lox");

        TINY_ASSERT_OK(! scanner2.hasError());
    }

    static void testScan()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner1("var this and or super break return", logger);
        auto tokens = scanner1.scanTokens();

        TINY_ASSERT_OK(tokens.size() == 8);

        TINY_ASSERT_EQUAL(tokens[0].type(), bodhi::TokenType::VAR);
        TINY_ASSERT_EQUAL(tokens[1].type(), bodhi::TokenType::THIS);
        TINY_ASSERT_EQUAL(tokens[2].type(), bodhi::TokenType::AND);
        TINY_ASSERT_EQUAL(tokens[3].type(), bodhi::TokenType::OR);
        TINY_ASSERT_EQUAL(tokens[4].type(), bodhi::TokenType::SUPER);
        TINY_ASSERT_EQUAL(tokens[5].type(), bodhi::TokenType::BREAK);
        TINY_ASSERT_EQUAL(tokens[6].type(), bodhi::TokenType::RETURN);
        TINY_ASSERT_EQUAL(tokens[7].type(), bodhi::TokenType::EndOfFile);

        TINY_ASSERT_OK(! scanner1.hasError());

        bodhi::Scanner scanner2("(( )){}))", logger);
        scanner2.scanTokens();
        TINY_ASSERT_OK(! scanner2.hasError());

        bodhi::Scanner scanner3("!*+-/=<> <= ==", logger);
        scanner3.scanTokens();
        TINY_ASSERT_OK(! scanner3.hasError());
    }

    static void testComment()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner1("// this is a valid comment", logger);
        scanner1.scanTokens();
        TINY_ASSERT_OK(! scanner1.hasError());

        bodhi::Scanner scanner2("// this is a\n// valid comment", logger);
        scanner2.scanTokens();
        TINY_ASSERT_OK(! scanner2.hasError());
    }
};
}

test::ScannerTest scannerTest;