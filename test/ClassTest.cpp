/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of lox-simple.

 lox-simple is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tiny-unit.hpp"

#include "../src/Interpreter.hpp"
#include "../src/Logger.hpp"
#include "../src/Parser.hpp"
#include "../src/Resolver.hpp"
#include "../src/RuntimeError.hpp"
#include "../src/Scanner.hpp"
#include "../src/Token.hpp"

#include <string>
#include <sstream>

namespace test
{
class ClassTest : public tiny::Unit
{
public:
    ClassTest()
    : tiny::Unit("ClassTest")
    {
        tiny::Unit::registerTest(&ClassTest::callMethod, "callMethod");
        tiny::Unit::registerTest(&ClassTest::createInstance, "createInstance");
        tiny::Unit::registerTest(&ClassTest::minimalPrint, "minimalPrint");
    }

    static std::shared_ptr<bodhi::Interpreter> makeInterpreter(const std::string& input)
    {
        std::stringstream buffer;
        bodhi::Logger logger(std::cout);
        bodhi::Scanner scanner(input, logger);

        auto tokens = scanner.scanTokens();
        TINY_ASSERT_OK(! scanner.hasError());

        bodhi::Parser parser(tokens, logger);

        auto statements = parser.parse();
        TINY_ASSERT_OK(not parser.m_hasError);

        auto interpreter = std::make_shared<bodhi::Interpreter>(logger);
        bodhi::Resolver resolver(*interpreter, logger);
        resolver.resolve(statements);

        TINY_ASSERT_OK(not logger.m_hadError);

        interpreter->interpret(statements);

        return interpreter;
    }

    static void callMethod()
    {
        auto interpreter = makeInterpreter(R"(
class Bacon {
  eat() {
    print "Crunch crunch crunch!";
  }
}

Bacon().eat(); // Prints "Crunch crunch crunch!".
        )");

        TINY_ASSERT_OK(not interpreter->hasError());
        TINY_ASSERT_EQUAL(interpreter->lastPrint(),  "Crunch crunch crunch!");
    }

    static void createInstance()
    {
        auto interpreter = makeInterpreter(R"(
class Bagel {
}
var bagel = Bagel();
print bagel; // Prints "Bagel instance".
        )");

        TINY_ASSERT_OK(not interpreter->hasError());
        auto bagel = interpreter->globals()->get("bagel");
        TINY_ASSERT_OK(bagel->isInstance());

        TINY_ASSERT_EQUAL(interpreter->lastPrint(),  "Bagel instance");
    }

    static void minimalPrint()
    {
        auto interpreter = makeInterpreter(R"(
class DevonshireCream {
  serveOn() {
    return "Scones";
  }
}

print DevonshireCream; // Prints "DevonshireCream".
        )");

        TINY_ASSERT_EQUAL(interpreter->lastPrint(), "DevonshireCream");
    }

};

ClassTest classTest;
}