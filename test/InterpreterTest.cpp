// /*
//  Copyright (C) 2017  Kim HOANG
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
// */
// #include "tiny-unit.hpp"

// #include "../src/Interpreter.hpp"
// #include "../src/Logger.hpp"
// #include "../src/Parser.hpp"
// #include "../src/Resolver.hpp"
// #include "../src/RuntimeError.hpp"
// #include "../src/Scanner.hpp"
// #include "../src/Token.hpp"

// #include <string>
// #include <sstream>

// namespace test {
// class InterpreterTest : public tiny::Unit
// {
// public:
//     InterpreterTest()
//     : tiny::Unit("InterpreterTest")
//     {
//         tiny::Unit::registerTest(&InterpreterTest::forLoop, "forLoop");
//         tiny::Unit::registerTest(&InterpreterTest::fibonacci, "fibonacci");
//         tiny::Unit::registerTest(&InterpreterTest::whileLoop, "whileLoop");
//         tiny::Unit::registerTest(&InterpreterTest::assignments, "assignments");
//         tiny::Unit::registerTest(&InterpreterTest::shortCircuit, "shortCircuit");
//         tiny::Unit::registerTest(&InterpreterTest::globalVars, "globalVars");
//         tiny::Unit::registerTest(&InterpreterTest::addUp, "addUp");
//         tiny::Unit::registerTest(&InterpreterTest::bookExample, "bookExample");
//         tiny::Unit::registerTest(&InterpreterTest::testBase, "testBase");
//     }

//     static std::shared_ptr<bodhi::Interpreter> makeInterpreter(const std::string& input)
//     {
//         std::stringstream buffer;
//         bodhi::Logger logger(buffer);
//         bodhi::Scanner scanner(input, logger);

//         auto tokens = scanner.scanTokens();
//         TINY_ASSERT_OK(! scanner.hasError());

//         bodhi::Parser parser(tokens, logger);

//         auto statements = parser.parse();
//         TINY_ASSERT_OK(not parser.m_hasError);

//         auto interpreter = std::make_shared<bodhi::Interpreter>(logger);
//         bodhi::Resolver resolver(*interpreter, logger);
//         resolver.resolve(statements);
//         TINY_ASSERT_OK(not logger.m_hadError);

//         interpreter->interpret(statements);

//         return interpreter;
//     }

//     static void forLoop()
//     {
//         auto interpreter = makeInterpreter(R"(
// var sum = 0;
// for (var i = 0; i < 10; i = i + 1)
// {
//     sum = sum + i;
// }
// print sum;
//         )");

//         TINY_ASSERT_OK(not interpreter->hasError());

//         auto sum = interpreter->globals()->get("sum");
//         TINY_ASSERT_OK(sum->isNumber());
//         TINY_ASSERT_EQUAL(sum->getNumber(), 45);
//         TINY_ASSERT_EQUAL(interpreter->lastPrint(), "45");
//     }

//     static void fibonacci()
//     {
//         auto interpreter = makeInterpreter(R"(
// var a = 0;
// var b = 1;

// while (b < 3) {
//   var temp = a;
//   a = b;
//   b = temp + b;
// }
//         )");

//         auto aVar = interpreter->environment()->get("a");
//         auto bVar = interpreter->environment()->get("b");

//         // the first two fibonacci numbers greater than 100
//         TINY_ASSERT_OK(aVar->isNumber());
//         TINY_ASSERT_EQUAL(aVar->getNumber(), 2);

//         TINY_ASSERT_OK(bVar->isNumber());
//         TINY_ASSERT_EQUAL(bVar->getNumber(), 3);

//     }

//     static void whileLoop()
//     {
//         auto interpreter = makeInterpreter(R"(
// var foo = 1;
// while(foo < 3) {
//     foo = foo + 1;
//     foo = 2 * foo;
// }
//         )");

//         auto foo = interpreter->environment()->get("foo");

//         TINY_ASSERT_OK(foo->isNumber());
//         TINY_ASSERT_EQUAL(foo->getNumber(), 4);
//     }

//     static void assignments()
//     {
//         auto interpreter = makeInterpreter("var foo = \"foo\";\nfoo = foo + \"bar\";\n");

//         auto foo = interpreter->environment()->get("foo");

//         TINY_ASSERT_OK(foo != nullptr);
//         TINY_ASSERT_OK(foo->isString());
//         TINY_ASSERT_EQUAL(foo->getString(), "foobar");
//     }

//     static void shortCircuit()
//     {
//         std::stringstream buffer;
//         bodhi::Logger logger(buffer);
//         bodhi::Scanner scanner("print \"hi\" or 2;", logger);

//         auto tokens = scanner.scanTokens();
//         bodhi::Parser parser(tokens, logger);

//         TINY_ASSERT_OK(! scanner.hasError());

//         auto statements = parser.parse();

//         bodhi::Interpreter interpreter(logger);
//         interpreter.interpret(statements);

//         TINY_ASSERT_EQUAL(interpreter.lastPrint(), "hi");
//     }

//     static void globalVars()
//     {
//         std::stringstream buffer;
//         bodhi::Logger logger(buffer);
//         bodhi::Scanner scanner("var a = 1;\nvar b = 2; var c = a + b;", logger);

//         auto tokens = scanner.scanTokens();
//         bodhi::Parser parser(tokens, logger);

//         TINY_ASSERT_OK(! scanner.hasError());

//         auto statements = parser.parse();

//         bodhi::Interpreter interpreter(logger);
//         interpreter.interpret(statements);

//         auto a = interpreter.environment()->get("a");

//         TINY_ASSERT_OK(a->isNumber());
//         TINY_ASSERT_EQUAL(a->getNumber(), 1.0);

//         auto c = interpreter.environment()->get("c");

//         TINY_ASSERT_OK(c->isNumber());
//         TINY_ASSERT_EQUAL(c->getNumber(), 3.0);

//     }

//     static void addUp()
//     {
//         std::stringstream buffer;
//         bodhi::Logger logger(buffer);
//         bodhi::Scanner scanner("1+2+3+4+5+6;\n", logger);

//         auto tokens = scanner.scanTokens();

//         TINY_ASSERT_EQUAL(tokens.size(), 13);

//         bodhi::Parser parser(tokens, logger);

//         TINY_ASSERT_OK(! scanner.hasError());

//         auto statements = parser.parse();

//         TINY_ASSERT_EQUAL(statements.size(), 1);
//         TINY_ASSERT_EQUAL(statements[0]->name(), "Expression");

//         bodhi::Interpreter interpreter(logger);
//         auto expr = statements[0]->getExpr();
//         TINY_ASSERT_OK(expr != nullptr);
//         auto value = interpreter.interpret(*expr);

//         TINY_ASSERT_OK(! value->isNil());
//         TINY_ASSERT_OK(value->isNumber());
//         TINY_ASSERT_EQUAL(value->getNumber(), 21.0);
//     }

//     static void bookExample()
//     {
//         std::stringstream buffer;
//         bodhi::Logger logger(buffer);

//         bodhi::Scanner scanner("2 * (3 / \"muffin\")", logger);
//         auto tokens = scanner.scanTokens();
//         bodhi::Parser parser(tokens, logger);

//         TINY_ASSERT_OK(! scanner.hasError());

//         auto expr = parser.parseExpression();

//         TINY_ASSERT_OK(expr != nullptr);
//         TINY_ASSERT_EQUAL(buffer.tellp(), 0);
//         bodhi::Interpreter interpreter(logger);

//         // TINY_ASSERT_TRY();
//         auto val =  interpreter.interpret(*expr);
//         // TINY_ASSERT_CATCH(bodhi::RuntimeError);
//         TINY_ASSERT_OK(val->isNil());
//     }

//     static void testBase()
//     {
//         std::stringstream buffer;
//         bodhi::Logger logger(buffer);

//         bodhi::Scanner scanner("-1/1", logger);
//         auto tokens = scanner.scanTokens();
//         bodhi::Parser parser(tokens, logger);

//         TINY_ASSERT_OK(! scanner.hasError());

//         auto expr = parser.parseExpression();

//         TINY_ASSERT_OK(expr != nullptr);

//         bodhi::Interpreter interpreter(logger);
//         auto result = interpreter.interpret(*expr);
//         TINY_ASSERT_EQUAL(buffer.tellp(), 0);
//         TINY_ASSERT_OK(result->isNumber());
//         TINY_ASSERT_EQUAL(result->getNumber(), -1);
//     }
// };
// }
// test::InterpreterTest interpreterTest;
