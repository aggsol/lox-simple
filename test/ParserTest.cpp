/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "tiny-unit.hpp"

#include "../src/Logger.hpp"
#include "../src/Scanner.hpp"
#include "../src/Parser.hpp"

class ParserTest : public tiny::Unit
{
public:
    ParserTest()
    : tiny::Unit("ParserTest")
    {
        tiny::Unit::registerTest(&ParserTest::oneAndOne, "oneAndOne");
        tiny::Unit::registerTest(&ParserTest::parseFunction, "parseFunction");
        tiny::Unit::registerTest(&ParserTest::print, "print");
        tiny::Unit::registerTest(&ParserTest::parserError, "parserError");
        tiny::Unit::registerTest(&ParserTest::parserWithScanner, "parserWithScanner");
    }

    static void oneAndOne()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner("1+1;\n", logger);

        auto tokens = scanner.scanTokens();
        bodhi::Parser parser(tokens, logger);

        TINY_ASSERT_OK(! scanner.hasError());

        auto statements = parser.parse();

        //std::cout << buffer.str() << std::endl;
        // TINY_ASSERT_EQUAL(buffer.tellp(), 0);
        TINY_ASSERT_EQUAL(statements.size(), 1);
    }

    static void parseFunction()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner("fun add(a,b) { print a+b; }\nadd(1,2);\n", logger);

        auto tokens = scanner.scanTokens();
        bodhi::Parser parser(tokens, logger);

        TINY_ASSERT_OK(! scanner.hasError());

        auto statements = parser.parse();

        // std::cout << buffer.str() << std::endl;
        TINY_ASSERT_EQUAL(buffer.tellp(), 0);
        TINY_ASSERT_EQUAL(statements.size(), 2);

        // for(auto s: statements)
        // {
        //     std::cout << s->name() << "\n";
        // }

        TINY_ASSERT_EQUAL(statements[0]->name(), "Function");
        TINY_ASSERT_EQUAL(statements[1]->name(), "Expression");
    }

    static void print()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner("print 12;\nprint true;\nprint \"hello\";\n", logger);

        auto tokens = scanner.scanTokens();
        bodhi::Parser parser(tokens, logger);

        TINY_ASSERT_OK(! scanner.hasError());

        auto statements = parser.parse();

        //std::cout << buffer.str() << std::endl;
        TINY_ASSERT_EQUAL(buffer.tellp(), 0);
        TINY_ASSERT_EQUAL(statements.size(), 3);
    }

    static void parserError()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner("var i=12\nvar true;", logger);

        auto tokens = scanner.scanTokens();
        bodhi::Parser parser(tokens, logger);

        TINY_ASSERT_OK(! scanner.hasError());
 
        auto statements = parser.parse();

        TINY_ASSERT_OK(parser.m_hasError);
    }

    static void parserWithScanner()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner("1/1\n1/1", logger);

        auto tokens = scanner.scanTokens();
        bodhi::Parser parser(tokens, logger);

        auto expr = parser.parseExpression();

        TINY_ASSERT_EQUAL(buffer.tellp(), 0);
        TINY_ASSERT_OK(expr != nullptr);
    }
};

ParserTest parserTest;