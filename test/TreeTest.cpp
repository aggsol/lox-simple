/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "tiny-unit.hpp"

#include "../src/ast/Tree.hpp"
#include "../src/ast/Literal.hpp"
#include "../src/Token.hpp"

class TreeTest : public tiny::Unit
{
public:
    TreeTest()
    : tiny::Unit("TreeTest")
    {
        tiny::Unit::registerTest(&TreeTest::basicInstance, "basicInstance");
    }

    static void basicInstance()
    {
        bodhi::Tree tree;
        
        auto foo = tree.createExpr<bodhi::Literal>("FOO");
        TINY_ASSERT_OK(foo != nullptr);
        
        auto num = tree.createExpr<bodhi::Literal>(9876.0);
        TINY_ASSERT_OK(num != nullptr);

        TINY_ASSERT_OK(foo->type() == bodhi::BaseExpr::Type::Literal);
        TINY_ASSERT_EQUAL(num->value().getDouble(), 9876.0);

        bodhi::Token minus(bodhi::TokenType::MINUS, "-", "", 1);

        bodhi::Unary(minus, *num);
    }
};

TreeTest TreeTest;