/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/Value.hpp"
#include "../src/LoxClass.hpp"
#include "../src/LoxInstance.hpp"

#include <memory>
#include <string>

class ValueTest : public tiny::Unit
{
public:
    ValueTest()
    : tiny::Unit("Value")
    {
        tiny::Unit::registerTest(&ValueTest::instances, "instances");
        tiny::Unit::registerTest(&ValueTest::copy, "copy");
        tiny::Unit::registerTest(&ValueTest::stream, "stream");
        tiny::Unit::registerTest(&ValueTest::testBase, "testBase");
    }

    static void instances()
    {
        std::unordered_map<std::string, bodhi::Value::Ptr> methods;
        auto klasse = std::make_shared<bodhi::LoxClass>("Klasse", methods);
        auto instance = std::make_shared<bodhi::LoxInstance>(klasse.get());

        bodhi::Value inst(instance);
        bodhi::Value kla(klasse);

        TINY_ASSERT_OK(inst.isInstance());
        TINY_ASSERT_OK(kla.isCallable());
    }

    static void copy()
    {
        bodhi::Value val1(1);

        bool isNumber = val1.isNumber();
        TINY_ASSERT_OK(isNumber);
        TINY_ASSERT_EQUAL(val1.getNumber(), 1.0);

        bodhi::Value val2(val1);

        isNumber = val1.isNumber();
        TINY_ASSERT_OK(isNumber);
        TINY_ASSERT_EQUAL(val1.getNumber(), 1.0);

        isNumber = val2.isNumber();
        TINY_ASSERT_OK(isNumber);
        TINY_ASSERT_EQUAL(val2.getNumber(), 1.0);
    }

    static void stream()
    {
        bodhi::Value value(true);
        TINY_ASSERT_OK(value.isTrue());

        std::ostringstream strm;
        strm << value;


        TINY_ASSERT_EQUAL(strm.str(), "true");

        bodhi::Value other(1.33);

        TINY_ASSERT_OK(value != other);

        value.assign(other);
        TINY_ASSERT_OK(value.isTrue());
        TINY_ASSERT_EQUAL(value, other);

        strm.str("");
        strm << value;
        TINY_ASSERT_EQUAL(strm.str(), "1.33");
    }

    static void testBase()
    {
        bodhi::Value value(true);

        TINY_ASSERT_OK(! value.isNil());

        value = bodhi::Value::getNil();

        TINY_ASSERT_OK(value.isNil());

        value.assign(3.1337);

        bool isNumber = value.isNumber();

        TINY_ASSERT_OK(isNumber);
        TINY_ASSERT_EQUAL(value.getNumber(), 3.1337);

        value.assign(false);

        bool isBool = value.isBool();

        TINY_ASSERT_OK(isBool);
        TINY_ASSERT_EQUAL(value.getBool(), false);

        value.assign("foobar");

        TINY_ASSERT_OK(value.isString());
        TINY_ASSERT_EQUAL(value.getString(), "foobar");
    }
};

ValueTest valueTest;