/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/CliArguments.hpp"

#include <string>
#include <stdexcept>

class CliArgumentsTest : public tiny::Unit
{
public:
    CliArgumentsTest()
    : tiny::Unit("CliArgumentsTest")
    {
        tiny::Unit::registerTest(&CliArgumentsTest::numbers, "numbers");
        tiny::Unit::registerTest(&CliArgumentsTest::wrongInit, "wrongInit");
        tiny::Unit::registerTest(&CliArgumentsTest::boolFlags, "boolFlags");
    }
    
    static void numbers()
    {
        const char* const argv[6] = { "foo", "--hex", "0xdeadbeef", "--count", "-12", "-n123" };
        bodhi::CliArguments args(6, argv);
        
        TINY_ASSERT_EQUAL(args.arguments().size(), 5);
        
        TINY_ASSERT_EQUAL(args.getOpt<float>("n", "", 13.37f), 123.0f);
        TINY_ASSERT_EQUAL(args.arguments().size(), 4);
        
        // TODO: this is weird behaviour
        TINY_ASSERT_EQUAL(args.getOpt<unsigned>("h", "hex", 1337), 0);
        TINY_ASSERT_EQUAL(args.arguments().size(), 2);
        
        TINY_ASSERT_EQUAL(args.getOpt<int>("", "count", -1337), -12);
        TINY_ASSERT_EQUAL(args.arguments().size(), 0);
    }
    
    static void wrongInit()
    {
        const char* const argv[2] = { "foo", "bar" };
        TINY_ASSERT_TRY();
            bodhi::CliArguments args(0, argv);
        TINY_ASSERT_CATCH(std::invalid_argument);
        
        TINY_ASSERT_TRY();
            bodhi::CliArguments args(2, nullptr);
        TINY_ASSERT_CATCH(std::invalid_argument);

    }

    static void boolFlags()
    {
        const char* const argv[3] = { "foo", "-bar", "--help" };
        bodhi::CliArguments args(3, argv);
        
        TINY_ASSERT_EQUAL(args.arguments().size(), 2);
        
        TINY_ASSERT_OK(args.getOpt("b", "beta"));
        TINY_ASSERT_OK(args.getOpt("r", ""));
        TINY_ASSERT_OK(args.getOpt("a", "alpha"));
        
        TINY_ASSERT_EQUAL(args.arguments().size(), 1);
        
        TINY_ASSERT_OK(args.getOpt("h", "help"));
        
        TINY_ASSERT_EQUAL(args.arguments().size(), 0);
    }
};

CliArgumentsTest cliArgumentsTest;
