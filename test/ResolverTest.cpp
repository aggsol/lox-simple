/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of lox-simple.

 lox-simple is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "tiny-unit.hpp"

#include "../src/Interpreter.hpp"
#include "../src/Logger.hpp"
#include "../src/Parser.hpp"
#include "../src/Resolver.hpp"
#include "../src/RuntimeError.hpp"
#include "../src/Scanner.hpp"
#include "../src/Token.hpp"

#include <string>
#include <sstream>

namespace test {
class ResolverTest : public tiny::Unit
{
public:
    ResolverTest()
    : tiny::Unit("ResolverTest")
    {
        tiny::Unit::registerTest(&ResolverTest::resolve, "resolve");
    }

    static std::shared_ptr<bodhi::Interpreter> makeInterpreter(const std::string& input)
    {
        std::stringstream buffer;
        bodhi::Logger logger(std::cout);
        bodhi::Scanner scanner(input, logger);

        auto tokens = scanner.scanTokens();
        TINY_ASSERT_OK(! scanner.hasError());

        // int i=1;
        // for(auto t: tokens)
        // {
        //     std::cout << i << ". Token: '" << t->lexeme() << "' '" << t->literal() << "'\n";
        //     ++i;
        // }

        bodhi::Parser parser(tokens, logger);

        auto statements = parser.parse();
        TINY_ASSERT_OK(not parser.m_hasError);

        auto interpreter = std::make_shared<bodhi::Interpreter>(logger);
        bodhi::Resolver resolver(*interpreter, logger);
        resolver.resolve(statements);

        TINY_ASSERT_OK(not logger.m_hadError);

        interpreter->interpret(statements);

        return interpreter;
    }

    static void resolve()
    {
        auto interpreter = makeInterpreter(R"(
var a = "outer";
{
  var a = "inner";
  print a;
}
        )");

        TINY_ASSERT_EQUAL(interpreter->lastPrint(), "inner");
    }
};
}
test::ResolverTest resolverTest;