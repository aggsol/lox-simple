#include "tiny-unit.hpp"

#include "../src/ValueNext.hpp"

class ValueNextTest : public tiny::Unit
{
public:
    ValueNextTest()
    : tiny::Unit("ValueNextTest")
    {
        tiny::Unit::registerTest(&ValueNextTest::boolAssignment, "boolAssignment");
        tiny::Unit::registerTest(&ValueNextTest::instances, "instances");
    }

    static void boolAssignment()
    {
        bodhi::ValueNext val;
        val.assign(false);
        bool b1 = val.getBool();
        TINY_ASSERT_EQUAL(b1, false);

        val.assign(true);
        bool b2 = val.getBool();
        TINY_ASSERT_EQUAL(b2, true);
    }

    static void instances()
    {
        bodhi::ValueNext nil;
        TINY_ASSERT_OK(nil.isNil());
        TINY_ASSERT_OK(not nil.isTruthy());

        bodhi::ValueNext b1(true);

        TINY_ASSERT_OK(b1.isBool());
        TINY_ASSERT_OK(not b1.isNil());
        TINY_ASSERT_OK(not b1.isNumber());
        TINY_ASSERT_OK(not b1.isString());
        TINY_ASSERT_OK(b1.isTruthy());

        bodhi::ValueNext b2(false);

        TINY_ASSERT_OK(b2.isBool());
        TINY_ASSERT_OK(not b2.isNil());
        TINY_ASSERT_OK(not b2.isNumber());
        TINY_ASSERT_OK(not b2.isString());
        TINY_ASSERT_OK(not b2.isTruthy());

        bodhi::ValueNext s("Foobar!");
        TINY_ASSERT_OK(not s.isBool());
        TINY_ASSERT_OK(not s.isNil());
        TINY_ASSERT_OK(not s.isNumber());
        TINY_ASSERT_OK(s.isString());
        TINY_ASSERT_OK(s.isTruthy());

        bodhi::ValueNext n(1337.0);
        TINY_ASSERT_OK(not n.isBool());
        TINY_ASSERT_OK(not n.isNil());
        TINY_ASSERT_OK(n.isNumber());
        TINY_ASSERT_OK(not n.isString());
        TINY_ASSERT_OK(n.isTruthy());

    }
};

ValueNextTest valueNextTest;