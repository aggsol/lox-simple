/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of lox-simple.

 lox-simple is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "tiny-unit.hpp"

#include "../src/Interpreter.hpp"
#include "../src/Logger.hpp"
#include "../src/Parser.hpp"
#include "../src/Resolver.hpp"
#include "../src/RuntimeError.hpp"
#include "../src/Scanner.hpp"
#include "../src/Token.hpp"

#include <string>
#include <sstream>

namespace test {
class FunctionTest : public tiny::Unit
{
public:
    FunctionTest()
    : tiny::Unit("FunctionTest")
    {
        tiny::Unit::registerTest(&FunctionTest::functionLoop, "functionLoop");
        tiny::Unit::registerTest(&FunctionTest::closure, "closure");
        tiny::Unit::registerTest(&FunctionTest::ch10Fibonacci, "ch10Fibonacci");
        tiny::Unit::registerTest(&FunctionTest::recursion, "recursion");
        tiny::Unit::registerTest(&FunctionTest::scanFunction, "scanFunction");
        tiny::Unit::registerTest(&FunctionTest::sayHi, "sayHi");
    }

    static std::shared_ptr<bodhi::Interpreter> makeInterpreter(const std::string& input)
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner(input, logger);

        auto tokens = scanner.scanTokens();
        TINY_ASSERT_OK(! scanner.hasError());

        bodhi::Parser parser(tokens, logger);

        auto statements = parser.parse();
        TINY_ASSERT_OK(not parser.m_hasError);

        auto interpreter = std::make_shared<bodhi::Interpreter>(logger);
        bodhi::Resolver resolver(*interpreter, logger);
        resolver.resolve(statements);

        TINY_ASSERT_OK(not logger.m_hadError);

        interpreter->interpret(statements);

        return interpreter;
    }

    static void functionLoop()
    {
        auto interpreter = makeInterpreter(R"(
var a=2;
fun mult(x, y)
{
    var tmp = x*y;
    return tmp;
}
while(2 == a)
{
    a = mult(a, 7);
}
        )");
        auto a = interpreter->globals()->get("a");
        TINY_ASSERT_OK(a->isNumber());
        TINY_ASSERT_EQUAL(a->getNumber(), 14);
    }

    static void closure()
    {
        auto interpreter = makeInterpreter(R"(
fun makePoint(x, y) {
  fun closure(method) {
    if (method == "x") return x;
    if (method == "y") return y;
    print "unknown method " + method;
  }

  return closure;
}

var point = makePoint(2, 3);
print point("x"); // "2".
print point("y"); // "3".
        )");

        auto makePoint = interpreter->globals()->get("makePoint");
        TINY_ASSERT_OK(makePoint->isCallable());

        auto point = interpreter->globals()->get("point");
        TINY_ASSERT_OK(point->isCallable());

        TINY_ASSERT_EQUAL(interpreter->lastPrint(), "3");
    }

    static void ch10Fibonacci()
    {
        auto interpreter = makeInterpreter(R"(
fun fibonacci(n) {
  if (n <= 1) return n;
  return fibonacci(n - 2) + fibonacci(n - 1);
}

for (var i = 0; i < 10; i = i + 1) {
  print fibonacci(i);
}
        )");

        TINY_ASSERT_EQUAL(interpreter->lastPrint(), "34"); // 10th fibonacci number including 0
    }

    static void recursion()
    {
        auto interpreter = makeInterpreter(R"(
fun foo(a)
{
    if(a > 2)
    {
        return 1;
    }
    return a + foo(a+1);
}
var r = foo(1);
print "r=" + r;
        )");

        TINY_ASSERT_EQUAL(interpreter->lastPrint(), "r=4"); // 1+2+1
    }

    static void scanFunction()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner("fun add(a,b) { print a+b; }\nadd(1,2);", logger);

        auto tokens = scanner.scanTokens();

        TINY_ASSERT_OK(! scanner.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 22);

        TINY_ASSERT_EQUAL(tokens[0].type(), bodhi::TokenType::FUN);

        bodhi::Parser parser(tokens, logger);
        auto statements = parser.parse();
    }


    static void sayHi()
    {
        auto interpreter = makeInterpreter(R"(
fun sayHi(first, last) {
  print "Hi, " + first + " " + last + "!";
}

sayHi("Dear", "Reader");
        )");

        TINY_ASSERT_EQUAL(interpreter->lastPrint(), "Hi, Dear Reader!");
    }
};
}

test::FunctionTest functionTest;