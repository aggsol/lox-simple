/**
* Tiny Unit Test Framework
* Author: Kim <foss@aggsol.de>
*
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* <foss@aggsol.de> wrote this file. As long as you retain this
* notice you can do whatever you want with this stuff. If we meet some day,
* and you think this stuff is worth it, you can buy me a beer in return KIM
* ----------------------------------------------------------------------------
*/
#ifndef TINY_UNIT_HPP_
#define TINY_UNIT_HPP_

#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>

/**
* Enable "not" keyword if /permissve- is nor available
*/
#if _MSC_VER
#include <ciso646>
#endif

/**
* Assertion macros for boolean expressions or equality tests.
*/
#define TINY_ASSERT_OK(expr) do { tiny::assertOk(expr, #expr, __FILE__, __LINE__); } while(false)
#define TINY_ASSERT_EQUAL(actual, expected) do { tiny::assertEqual(actual, expected, __FILE__, __LINE__); } while(false)

/**
* Assertion macros for expected exceptions
*/
#define TINY_ASSERT_TRY() do { bool tiny_exception = false; try {
#define TINY_ASSERT_CATCH(type) } catch (const type &) { tiny_exception = true; } if( tiny_exception == false ) { tiny::handleMissedException(#type, __FILE__, __LINE__); } } while(false)

namespace tiny
{
    /**
    * Function pointer type for test cases
    */
    typedef void (*TestFunc) (void);

    /**
    * A Unit contains test cases. Every test case has to be registered.
    */
    class Unit
    {
    public:
        explicit Unit(const std::string& name);

        void registerTest(TestFunc foo, const std::string& testName);
        unsigned runTests(const std::string& name);
    protected:
        virtual ~Unit() {}

    private:
        struct TestCase
        {
            TestFunc foo { nullptr };
            std::string name {} ;
        };

        Unit() = delete;
        Unit(const Unit& other) = delete;
        Unit(Unit&& other) = delete;

        std::vector< TestCase > m_testCases = {};

        std::string m_name;
    };

    /**
    * Exception if a test failed, other exceptions must be catched or
    * the test will fail.
    */
    class TestFailed : public std::exception
    {
    public:
        explicit TestFailed(const std::string& msg)
        : m_message(msg)
        {}

        ~TestFailed() throw() override {}

        const char* what() const throw() override
        {
            return m_message.c_str();
        }

    private:
        TestFailed();
        std::string m_message;
    };

    /**
    * Basic assertion for testing
    */
    void assertOk(bool expr, const char* rep, const char* filename, unsigned line);

    /**
    * Assertion for equality
    */
    template< typename U, typename V >
    void assertEqual(const U& actual, const V& expected, const char* filename, unsigned line)
    {
        if( !(actual == static_cast<U>(expected)) )
        {
            std::ostringstream msg;
            msg << filename << ":" << line
                << ": Not equal. Expected=<" << expected << "> Actual=<" << actual << ">";
            throw TestFailed( msg.str() );
        }
    }

    void handleMissedException(const std::string& type, const char* filename, unsigned line);
}

#endif
