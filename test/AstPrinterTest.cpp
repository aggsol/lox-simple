/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/ast/Printer.hpp"
#include "../src/Token.hpp"
#include "../src/ast/Literal.hpp"
#include "../src/ast/BaseExpr.hpp"

#include <cstdlib>
#include <iostream>
#include <memory>

using namespace bodhi;

class AstPrinterTest : public tiny::Unit
{
public:
    AstPrinterTest()
    : tiny::Unit("AstPrinterTest")
    {
        tiny::Unit::registerTest(&AstPrinterTest::testString, "testString");
        tiny::Unit::registerTest(&AstPrinterTest::testBookExample, "testBookExample");
    }

    static void testBookExample()
    {
        bodhi::Token minus(TokenType::MINUS, "-", "", 1);
        bodhi::Token star(TokenType::STAR, "*", "", 1);

        bodhi::Literal a(123.0);
        bodhi::Literal b(45.67);
        bodhi::Unary unary(minus,a);

        bodhi::Grouping grp(b);

        bodhi::Binary binary(unary, star, grp);

        bodhi::Printer printer;
        auto result = printer.print(binary);

        TINY_ASSERT_EQUAL(result, "(* (- 123) (group 45.67))");
    }

    static void testString()
    {
        bodhi::Literal literal("FooBar!");

        bodhi::Printer printer;
        auto result = printer.print(literal);

        TINY_ASSERT_EQUAL(result, "FooBar!");

        bodhi::Token minus(TokenType::MINUS, "-", "", 1);
        bodhi::Literal number(1337.0);
        bodhi::Unary unary(minus,number);

        result = printer.print(unary);

        TINY_ASSERT_EQUAL(result, "(- 1337)");
    }
};

AstPrinterTest astPrinterTest;
