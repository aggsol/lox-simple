/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/Expression.hpp"

#include <string>

using namespace bodhi;
using namespace bodhi::Expr;

class ExpressionTest : public tiny::Unit
{
public:
    ExpressionTest()
    : tiny::Unit("ExpressionTest")
    {
        tiny::Unit::registerTest(&ExpressionTest::LiteralNumber, "Literal Number");

        tiny::Unit::registerTest(&ExpressionTest::LiteralBool, "Literal Bool");
        tiny::Unit::registerTest(&ExpressionTest::LiteralString, "Literal String");
    }

    static void LiteralNumber()
    {
        Literal literal(1.1111);

        bool result = literal.value()->isNumber();

        TINY_ASSERT_OK(! literal.value()->isNil());
        TINY_ASSERT_OK(result);
        TINY_ASSERT_EQUAL(literal.value()->getNumber(), 1.1111);

        TINY_ASSERT_OK(! literal.value()->isBool());
    }

    static void LiteralBool()
    {
        Literal literal(false);

        bool result = literal.value()->isBool();

        TINY_ASSERT_OK(! literal.value()->isNil());
        TINY_ASSERT_OK(result);
        TINY_ASSERT_EQUAL(literal.value()->getBool(), false);

        TINY_ASSERT_OK(! literal.value()->isNumber());
    }

    static void LiteralString()
    {
        auto literal = std::make_shared<Literal>("foobar");

        bool result = literal->value()->isString();

        TINY_ASSERT_OK(result);
        TINY_ASSERT_EQUAL(literal->value()->getString(), "foobar");

        TINY_ASSERT_OK(! literal->value()->isNil());

        TINY_ASSERT_OK(! literal->value()->isNumber());
    }
};

ExpressionTest expressionTest;