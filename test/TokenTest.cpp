/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/TokenType.hpp"

#include <iostream>
#include <sstream>

class TokenTest : public tiny::Unit
{
public:
    TokenTest()
    : tiny::Unit("TokenTest")
    {
        tiny::Unit::registerTest(&TokenTest::stream, "stream");
        tiny::Unit::registerTest(&TokenTest::namesOrder, "namesOrder");
    }
    
    static void stream()
    {
        std::ostringstream buffer;
        buffer << bodhi::TokenType::SLASH;
        
        TINY_ASSERT_EQUAL(buffer.str(), "slash");
    }
    
    static void namesOrder()
    {
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::STAR), "star");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::PLUS_PLUS), "increment");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::PLUS), "plus");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::MINUS_MINUS), "decrement");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::MINUS), "minus");
        
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::EndOfFile), "eof");
    }

};

TokenTest tokenTest;