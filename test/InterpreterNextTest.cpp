/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "tiny-unit.hpp"

#include "../src/InterpreterNext.hpp"
#include "../src/Logger.hpp"
#include "../src/Scanner.hpp"
#include "../src/ParserNext.hpp"
#include "../src/ast/Tree.hpp"
#include "../src/ast/BaseExpr.hpp"
#include "../src/ValueNext.hpp"

#include <sstream>
#include <string>

class InterpreterNextTest : public tiny::Unit
{
public:
    InterpreterNextTest()
    : tiny::Unit("InterpreterNextTest")
    {
        tiny::Unit::registerTest(&InterpreterNextTest::UnaryMinus, "UnaryMinus");
    }

    static void UnaryMinus()
    {
        bodhi::Tree tree;
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner("-123;", logger);

        auto tokens = scanner.scanTokens();
        TINY_ASSERT_OK(! scanner.hasError());
        TINY_ASSERT_OK(tokens.size() > 0);

        bodhi::ParserNext parser(tree, tokens, logger);

        auto expr = parser.parseExpression();

        TINY_ASSERT_OK(expr != nullptr);
        TINY_ASSERT_OK(expr->type() == bodhi::BaseExpr::Type::Unary);

        bodhi::InterpreterNext interpreter;
        auto val = interpreter.evaluate(*expr);

        TINY_ASSERT_OK(val.isNumber());
        TINY_ASSERT_EQUAL(val.getDouble(), -123.0);
    }
};

InterpreterNextTest interpreterNextTest;