/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/ClockFunction.hpp"
#include "../src/Environment.hpp"
#include "../src/Value.hpp"
#include "../src/RuntimeError.hpp"
#include "../src/Token.hpp"
#include "../src/TokenType.hpp"

#include <memory>

class EnvironmentTest : public tiny::Unit
{
public:
    EnvironmentTest()
    : tiny::Unit("EnvironmentTest")
    {
        tiny::Unit::registerTest(&EnvironmentTest::nullDefine, "nullDefine");
        tiny::Unit::registerTest(&EnvironmentTest::envInstances, "envInstances");
        tiny::Unit::registerTest(&EnvironmentTest::globalCallable, "globalCallable");
        tiny::Unit::registerTest(&EnvironmentTest::nesting, "nesting");
        tiny::Unit::registerTest(&EnvironmentTest::assignment, "assignment");
        tiny::Unit::registerTest(&EnvironmentTest::loadStore, "loadStore");
    }

    static void nullDefine()
    {
        auto global = std::make_shared<bodhi::Environment>();
        global->define("Boo");

        auto boo = global->get("Boo");
        TINY_ASSERT_OK(boo == nullptr);
    }

    static void envInstances()
    {
        auto global = std::make_shared<bodhi::Environment>();
        auto outer = std::make_shared<bodhi::Environment>(global);
        auto inner = std::make_shared<bodhi::Environment>(outer);

        global->define("a", 1);
        inner->define("a", 100);

        auto innerA = inner->get("a");
        TINY_ASSERT_OK(innerA != nullptr);
        innerA->assign( 2.0 );

        auto globalA = global->get("a");
        globalA->assign( 3.0 );

        innerA = inner->get("a");
        TINY_ASSERT_OK(innerA != nullptr);
        innerA->assign( 4.0 );

        TINY_ASSERT_EQUAL(global->get("a")->getNumber(), 3.0);
        TINY_ASSERT_EQUAL(inner->get("a")->getNumber(), 4.0);
    }

    static void globalCallable()
    {
        auto global = std::make_shared<bodhi::Environment>();
        auto outer = std::make_shared<bodhi::Environment>(global);
        auto inner = std::make_shared<bodhi::Environment>(outer);

        auto clockFunc = std::make_shared<bodhi::ClockFunction>();
        global->define("clock", clockFunc);

        auto val = inner->get("clock");

        TINY_ASSERT_OK(val != nullptr);
        TINY_ASSERT_OK(val->isCallable());

        auto fun = val->getCallable();

        TINY_ASSERT_OK(fun != nullptr);
        TINY_ASSERT_EQUAL(fun->arity(), 0);
        TINY_ASSERT_EQUAL(fun->toString(), "<fn clock>");
    }

    static void nesting()
    {
        auto global = std::make_shared<bodhi::Environment>();
        auto scope1 = std::make_shared<bodhi::Environment>(global);
        auto scope2 = std::make_shared<bodhi::Environment>(scope1);

        bodhi::Token token(bodhi::TokenType::IDENTIFIER,
            "a", "var", 1);

        global->define("a", 1337);

        auto val = scope2->get(&token);

        TINY_ASSERT_OK(val->isNumber());
        TINY_ASSERT_EQUAL(val->getNumber(), 1337.0);
    }

    static void assignment()
    {
        bodhi::Environment env;

        bodhi::Token token(bodhi::TokenType::IDENTIFIER,
            "foo", "foo", 1);

        auto nil = std::make_shared<bodhi::Value>();
        env.define("foo", nil);

        auto eleven = std::make_shared<bodhi::Value>(11);
        env.assign(&token, eleven);

        auto other = env.get(&token);

        TINY_ASSERT_OK(other->isNumber());
        TINY_ASSERT_EQUAL(other->getNumber(), 11);

        bodhi::Token token2 (bodhi::TokenType::IDENTIFIER, "bar", "bar", 1);

        TINY_ASSERT_TRY();
            env.assign(&token2, other);
        TINY_ASSERT_CATCH(bodhi::RuntimeError);
    }

    static void loadStore()
    {
        bodhi::Environment env;

        auto val1 = std::make_shared<bodhi::Value>(12);

        env.define("val1", val1);

        bodhi::Token token(bodhi::TokenType::IDENTIFIER,
            "val1", "foo", 1);

        auto val2 = env.get(&token);

        bool isNumber = val1->isNumber();
        TINY_ASSERT_OK(isNumber);
        TINY_ASSERT_EQUAL(val1->getNumber(), 12.0);

        isNumber = val2->isNumber();
        TINY_ASSERT_OK(isNumber);
        TINY_ASSERT_EQUAL(val2->getNumber(), 12.0);

        bodhi::Token unknown(bodhi::TokenType::IDENTIFIER,
            "unknown", "foo", 1);

        TINY_ASSERT_TRY();
            env.get(&unknown);
        TINY_ASSERT_CATCH(bodhi::RuntimeError);
    }
};


EnvironmentTest environment;