/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "tiny-unit.hpp"

#include "../src/Logger.hpp"
#include "../src/Scanner.hpp"
#include "../src/ParserNext.hpp"
#include "../src/ast/Tree.hpp"

class ParserNextTest : public tiny::Unit
{
public:
    ParserNextTest()
    : tiny::Unit("ParserNextTest")
    {
        tiny::Unit::registerTest(&ParserNextTest::missingLeftOperand, "missingLeftOperand");
        tiny::Unit::registerTest(&ParserNextTest::basicInstance, "basicInstance");
    }

    static void missingLeftOperand()
    {        
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        {
            bodhi::Scanner scanner("+ 1;\n", logger);
            bodhi::Tree tree;

            auto tokens = scanner.scanTokens();
            bodhi::ParserNext parser(tree, tokens, logger);      
            auto result = parser.parseExpression();        
            TINY_ASSERT_OK(result == nullptr);
        }
        {
            bodhi::Scanner scanner("<<2;\n", logger);
            bodhi::Tree tree;

            auto tokens = scanner.scanTokens();
            bodhi::ParserNext parser(tree, tokens, logger);      
            auto result = parser.parseExpression();        
            TINY_ASSERT_OK(result == nullptr);
        }
        {
            bodhi::Scanner scanner("!= \"foo\";\n", logger);
            bodhi::Tree tree;

            auto tokens = scanner.scanTokens();
            bodhi::ParserNext parser(tree, tokens, logger);      
            auto result = parser.parseExpression();        
            TINY_ASSERT_OK(result == nullptr);
        }

    }

    static void basicInstance()
    {
        std::stringstream buffer;
        bodhi::Logger logger(buffer);
        bodhi::Scanner scanner("1+1;\n", logger);
        bodhi::Tree tree;

        auto tokens = scanner.scanTokens();
        bodhi::ParserNext parser(tree, tokens, logger);

        auto result = parser.parseExpression();
        TINY_ASSERT_OK(result != nullptr);
    }
};

ParserNextTest parserNextTest;