/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <iomanip>

#include <BuildInfo.hpp>

#include "CliArguments.hpp"
#include "Interpreter.hpp"
#include "Logger.hpp"
#include "Parser.hpp"
#include "Resolver.hpp"
#include "Scanner.hpp"
#include "Token.hpp"
#include "TokenType.hpp"
#include "Value.hpp"

void run(const std::string& source)
{
    bodhi::Logger logger;
    bodhi::Scanner scanner(source, logger);
    std::vector<bodhi::Token> tokens = scanner.scanTokens();

    if(scanner.hasError()) return;

    bodhi::Parser parser(tokens, logger);
    auto statements = parser.parse();

    bodhi::Interpreter interpreter(logger);
    bodhi::Resolver resolver(interpreter, logger);
    resolver.resolve(statements);

    interpreter.interpret(statements);
}

void runPrompt()
{
    bodhi::Logger logger;
    bodhi::Interpreter interpreter(logger);

    std::string line;
    while (true)
    {
        std::cout << "> ";

        std::getline(std::cin, line);

        if(line == "exit" || line == "quit") return;

        bodhi::Scanner scanner(line, logger);
        std::vector<bodhi::Token> tokens = scanner.scanTokens();

        bodhi::Parser parser(tokens, logger);

        auto statements = parser.parse();

        if(statements.size() == 1 && statements[0]->name() == "Expression")
        {
            auto expr = statements[0]->getExpr();
            assert(expr != nullptr);
            auto value = interpreter.interpret(*expr);
            std:: cout << *value << "\n";
        }
        else
        {
            interpreter.interpret(statements);
        }

    }
}

void runFile(const std::string& filename)
{
    std::ifstream ifs(filename);
    std::ostringstream source;
    source << ifs.rdbuf();

    run(source.str());
}

void printUsage()
{
    std::cout << "Usage:\n";
    std::cout << "  " << BUILD_NAME << "                Start REPL loop\n";
    std::cout << "  " << BUILD_NAME << " <PATH>         Execute file\n";
    std::cout << "  " << BUILD_NAME << " <OPTIONS>\n";
    std::cout << "Options:\n";
    std::cout << "  -h, --help              Print this help\n";
    std::cout << "      --version           Print version\n";
}

int main(int argc, char *argv[])
{
    std::ios_base::sync_with_stdio(false);

    bodhi::CliArguments args(argc, argv);
    bool help = args.getOpt("h", "help");
    bool version = args.getOpt("", "version");

    if(help)
    {
        printUsage();
        return 0;
    }

    if(version)
    {
        std::cout << BUILD_NAME << " Version " << BUILD_VERSION <<
            " Build " << BUILD_TIMESTAMP << " " << BUILD_TYPE << "\n";
        return 0;
    }

    try
    {
        if(args.arguments().size() == 1)
        {
            runFile(args.arguments().at(0));
        }
        else
        {
            runPrompt();
        }
    }
    catch(const std::exception& ex)
    {
        std::cerr << "Uncatched expection: " << ex.what() << std::endl;
        return 101;
    }

    return 0;
}
