/*
 Copyright (C) 2020  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "InterpreterNext.hpp"
#include "TokenType.hpp"
#include "ValueNext.hpp"
#include "ast/BaseExpr.hpp"
#include "Token.hpp"

namespace bodhi
{
    ValueNext InterpreterNext::visitLiteralExpr(Literal& literal)
    {
        return literal.value();
    }

    ValueNext InterpreterNext::visitGroupingExpr(Grouping& grp)
    {
        return evaluate(grp.expression);
    }

    ValueNext InterpreterNext::visitUnaryExpr(Unary& unary)
    {
        auto right = evaluate(unary.right);

        assert(not right.isNil());

        switch(unary.op.type())
        {
            case TokenType::MINUS:
            return ValueNext(-right.getDouble());
            
            case TokenType::BANG:
            return ValueNext(not right.isTruthy());
            default:
            
            std::cout << "Unhandled unary op: '" << unary.op.type() << "'\n";
            assert(false);
        }

        return ValueNext();
    }

    ValueNext InterpreterNext::evaluate(BaseExpr& expr)
    {
        switch(expr.type())
        {
            case BaseExpr::Type::Literal:
            return visitLiteralExpr(static_cast<Literal&>(expr));
            
            case BaseExpr::Type::Grouping:
            return visitGroupingExpr(static_cast<Grouping&>(expr));
            
            case BaseExpr::Type::Unary:
            return visitUnaryExpr(static_cast<Unary&>(expr));

            default:
            std::cerr << "InterpreterNext::evaluate() Unhandled expression\n";
            assert(false);
        }
    }

}