/*
 Copyright (C) 2020  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ValueNext.hpp"

#include <cassert>
#include <iostream>
#include <iomanip>

namespace bodhi
{
    // -- CTORS --

    ValueNext::ValueNext()
    : m_tag(Tag::Nil)
    {}

    ValueNext::ValueNext(const char* str)
    : ValueNext(std::string(str))
    {}

    ValueNext::ValueNext(const std::string& value)
    : m_tag(Tag::String)
    , m_value(value)
    {}

    ValueNext::ValueNext(bool value)
    {
        if(value)
        {
            m_tag = Tag::True;
        }
        else
        {
            m_tag = Tag::False;
        }
    }

    ValueNext::ValueNext(int i)
    : ValueNext(static_cast<double>(i))
    {}

    ValueNext::ValueNext(double value)
    : m_tag(Tag::Number)
    , m_value(value)
    { }

    // -- Getters

    bool ValueNext::getBool() const
    {
        assert(m_tag == Tag::True || m_tag == Tag::False);
        return m_tag == Tag::True;
    }

    double ValueNext::getDouble() const
    {
        assert(m_tag == Tag::Number);
        return std::get<double>(m_value);
    }

    std::string ValueNext::getString() const
    {
        assert(m_tag == Tag::String);
        return std::get<std::string>(m_value);
    }

    // -- Assignments
    
    void ValueNext::assign(const ValueNext& other)
    {
        m_tag = other.m_tag;
        m_value = other.m_value;
    }

    void ValueNext::assign(double val)
    {
        m_tag = Tag::Number;
        m_value = val;        
    }

    void ValueNext::assign(bool val)
    {
        if(val)
        {
            m_tag = Tag::True;
        }
        else
        {
            m_tag = Tag::False;
        }
    }

    void ValueNext::assign(const std::string& val)
    {
        m_tag = Tag::String;
        m_value = val;
    }

    void ValueNext::assign(const char* val)
    {
        m_tag = Tag::String;
        m_value = val;
    }

    // -- Operators

    std::ostream& operator<<(std::ostream& os, const ValueNext& val)
    {
        switch(val.m_tag)
        {
        case ValueNext::Tag::Nil:
            os << "nil";
        break;
        case ValueNext::Tag::Number:
            os << std::setprecision(std::numeric_limits<double>::digits10) 
            << val.getDouble();
        break;
        case ValueNext::Tag::String:
            os << val.getString();
        break;
        case ValueNext::Tag::True:
            os << "true";
        break;
        case ValueNext::Tag::False:
            os << "false";
        break;
        case ValueNext::Tag::Callable:
            // os << m_callable->toString();
        break;
        case ValueNext::Tag::Instance:            
            // os << m_instance->toString();
        break;
        default:
            assert(false);
        break;
        }

        // os << "@0x" << std::setfill('0') << std::setw(8) << std::hex << &val;

        return os;
    }
}