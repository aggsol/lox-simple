#include "TokenType.hpp"

#include <array>
#include <string>

namespace bodhi
{
    namespace
    {
        std::array<std::string, 43> s_name = {
            {
            "INVALID"
            ,"left parenthesis"
            ,"right parenthesis"
            ,"left brace"
            ,"right brace"
            ,"comma"
            ,"dot"
            ,"semicolon"
            ,"slash"
            ,"star"

            ,"bang"
            ,"bang equal"
            ,"equal"
            ,"equal equal"
            ,"greater"
            ,"greater equal"
            ,"less"
            ,"less equal"
            ,"minus"
            ,"plus"
            ,"decrement"
            ,"increment"

            ,"identifier"
            ,"string"
            ,"number"
            ,"and"
            ,"class"
            ,"else"
            ,"false"
            ,"fun"
            ,"for"
            ,"if"
            ,"nil"
            ,"or"
            ,"print"
            ,"return"
            ,"super"
            ,"this"
            ,"true"
            ,"var"
            ,"while"
            ,"break"
            ,"eof"}
        };
    }

    std::string convertToString(TokenType type)
    {
        return s_name.at(static_cast<int>(type));
    }

    std::ostream& operator<<(std::ostream& os, const TokenType& fr)
    {
        os <<  s_name.at(static_cast<int>(fr));
        return os;
    }

}