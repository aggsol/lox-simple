/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Environment.hpp"
#include "RuntimeError.hpp"

#include <cassert>
#include <sstream>

namespace bodhi
{
    Environment::Environment(Environment::Ptr other)
    : m_enclosing(other)
    , m_depth(other->m_depth + 1)
    {
        // std::cout << "Created Environment m_depth=" << m_depth << "\n";
    }

    Environment::Environment()
    : m_enclosing(nullptr)
    , m_depth(0)
    {
        // std::cout << "Created Environment m_depth=" << m_depth << "\n";
    }

    void Environment::assignAt(int dist, Token::Ptr name, Value::Ptr val)
    {
        assert(val != nullptr);
        assert(name != nullptr);

        // std::cout << "Environment::assignAt " << name->lexeme() << " = " << *val
        // << " dist: " << dist
        // << " m_depth=" << m_depth
        // << "\n";
        ancestor(dist)->m_values[name->lexeme()] = val;
    }

    void Environment::assign(Token::Ptr name, ICallable::Ptr callable)
    {
        assert(callable != nullptr);
        assert(name != nullptr);
        assign(name, std::make_shared<Value>(callable));
    }


    void Environment::assign(Token::Ptr name, Value::Ptr val)
    {
        assert(val != nullptr);
        assert(name != nullptr);

        auto it = m_values.find(name->lexeme());
        if(it != m_values.end())
        {
            m_values[name->lexeme()] = val;
            //std::cout << "Environment::assign " << name->lexeme() << " = " << *val << " depth: " << depth() << "\n";
            return;
        }

        if(m_enclosing != nullptr)
        {
            m_enclosing->assign(name, val);
            return;
        }

        std::ostringstream msg;
        msg << "Undefined variable '" << name->lexeme() << "'. m_depth=" << m_depth;
        //std::cout << msg.str() << "\n";
        throw RuntimeError(msg.str(), *name);
    }

    void Environment::define(const std::string& name)
    {
        assert(not name.empty());
        assert(m_values.find(name) == m_values.end());

        m_values[name] = nullptr;
    }

    void Environment::define(const std::string& name, Value::Ptr val)
    {
        assert(val != nullptr);
        assert(not name.empty());
        assert(m_values.find(name) == m_values.end());

        m_values[name] = std::make_shared<Value>(*val);

        // std::cout << "Environment::define var " << name << " = " << *val
        //     << " m_depth=" << m_depth << "\n";
    }

    void Environment::define(const std::string& name, int val)
    {
        assert(not name.empty());
        assert(m_values.find(name) == m_values.end());

        m_values[name] = std::make_shared<Value>(val);

        //std::cout << "Environment::define var " << name << " = " << val << "\n";

    }

    void Environment::define(const std::string& name, ICallable::Ptr callable)
    {
        assert(not name.empty());
        assert(callable != nullptr);
        m_values[name] = std::make_shared<Value>(callable);
    }

    Value::Ptr Environment::get(const std::string& name) const
    {
        auto it = m_values.find(name);
        if(it != m_values.end())
        {
            return it->second;
        }

        if(m_enclosing != nullptr)
        {
            std::cout << "Environment::get Not found '" << name << "'. m_depth=" << m_depth << "\n";
            auto val = m_enclosing->get(name);
            return val;
        }

        std::ostringstream msg;
        msg << "Undefined variable '" << name << "'. m_depth=" << m_depth;
        throw RuntimeError(msg.str());

        return nullptr;
    }

    Value::Ptr Environment::get(Token::Ptr name) const
    {
        return get(name->lexeme());
    }

    Value::Ptr Environment::getAt(int dist, const std::string& name)
    {
        auto& values = ancestor(dist)->m_values;
        auto it = ancestor(dist)->m_values.find(name);
        if(it == values.end())
        {
            std::ostringstream msg;
            msg << "Undefined variable '" << name << "' at distance " << dist << " m_depth=" << m_depth;
            throw RuntimeError(msg.str());
        }
        assert(it->second != nullptr);
        return it->second;
    }

    Environment* Environment::ancestor(int dist)
    {
        auto env = this;
        for(int i=0; i<dist; ++i)
        {
            assert(env != nullptr);
            assert(env->m_enclosing != nullptr);
            env = env->m_enclosing.get();
        }
        return env;
    }
}