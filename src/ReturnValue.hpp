/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/

#ifndef BODHI_RETURN_VALUE_HPP
#define BODHI_RETURN_VALUE_HPP

#include "Value.hpp"

#include <exception>

namespace bodhi
{
    /**
     * Note: This hurts so much, I would NEVER use exceptions for control flow.
     * This is how the book structures it because it uses an implicit call stack.
     **/
    class ReturnValue : public std::exception
    {
    public:
        explicit ReturnValue(Value::Ptr value)
        : m_value(value)
        {}

        // virtual const char* what() const throw()
        // {
        //     return "return value";
        // }

        Value::Ptr  m_value;
    };
}

#endif