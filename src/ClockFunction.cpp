/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ClockFunction.hpp"

#ifdef __linux__
#include <time.h>
#endif

namespace bodhi
{

    Value::Ptr ClockFunction::call(Interpreter& , const std::deque<Value::Ptr>& )
    {
#ifdef __linux__
        unsigned long ns;
        struct timespec spec;

        clock_gettime(CLOCK_REALTIME , &spec);

        //                  milli  mikro  nano
        ns = (spec.tv_sec * 1000 * 1000 * 1000) + spec.tv_nsec;

        double s = static_cast<double>(ns) / 1000.0 / 1000.0 / 1000.0;
        return std::make_shared<Value>(s);
#else
        return std::make_shared<Value>(1337.1337);
#endif
    }

    std::string ClockFunction::toString() const
    {
        return "<fn clock>";
    }
}