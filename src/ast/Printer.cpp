/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "Printer.hpp"
#include "BaseExpr.hpp"
#include "Literal.hpp"
#include "../Token.hpp"

#include <iostream>
#include <sstream>
#include <string>

namespace bodhi
{
    // TODO: could expr be const?
    std::string Printer::print(const BaseExpr& expr)
    {     
        switch(expr.type())
        {
            case BaseExpr::Type::Assignment:
            return printAssignment(static_cast<const Assignment&>(expr));
            
            case BaseExpr::Type::Binary:
            return printBinary(static_cast<const Binary&>(expr));

            case BaseExpr::Type::Literal:
            return printLiteral(static_cast<const Literal&>(expr));

            case BaseExpr::Type::Unary:
            return printUnary(static_cast<const Unary&>(expr));

            case BaseExpr::Type::Grouping:
            return printGrouping(static_cast<const Grouping&>(expr));

            case BaseExpr::Type::INVALID:
            default:
                std::cerr << "Unhandled expression: " << expr.typeName() << "\n";
        }
        return "XXX";
    }

    std::string Printer::parenthesize(const std::string& name, BaseExpr& left, 
    BaseExpr& right)
    {
        std::ostringstream builder;
        builder << "(" << name << " " << print(left) << " " << print(right) << ")";
        return builder.str();
    }

    std::string Printer::printAssignment(const Assignment& assign)
    {
        return "TODO printAssignment";
    }

    std::string Printer::printBinary(const Binary& binary)
    {     
        return parenthesize(binary.op.lexeme(), binary.left, binary.right);
    }

    std::string Printer::printGrouping(const Grouping& grp)
    {     
        std::ostringstream builder;
        builder << "(group " << print(grp.expression) << ")";
        return builder.str();    
    }

    std::string Printer::printLiteral(const Literal& literal)
    {
        auto val = literal.value();     

        std::ostringstream builder;
        builder << val;
        return builder.str();        
    }

    std::string Printer::printUnary(const Unary& unary)
    {     
        std::ostringstream builder;
        builder << "(" << unary.op.lexeme() << " " << print(unary.right) << ")";
        return builder.str();       
    }
}