/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "BaseExpr.hpp"

#include <memory>
#include <vector>

namespace bodhi {

/*
    Container for the complete tree
*/
class Tree
{
public:
    template<typename T, class... Args>
    T*   createExpr(Args&&... args)
    {
        auto expr = std::unique_ptr<T>(new T(std::forward<Args>(args)...));
        m_expressions.push_back(std::move(expr));
        return static_cast<T*>(m_expressions.back().get());
    }

private:
    std::vector<std::unique_ptr<BaseExpr>>  m_expressions;
};
}