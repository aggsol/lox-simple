/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#include <string>

namespace bodhi
{
class BaseExpr;
class Binary;
class Assignment;
class Grouping;
class Literal;
class Unary;

class Printer
{
public:
    std::string print(const BaseExpr& expr);
private:
    std::string parenthesize(const std::string& name, BaseExpr& left, BaseExpr& right);
    
    std::string printAssignment(const Assignment& assign);
    std::string printBinary(const Binary& binary);
    std::string printGrouping(const Grouping& grp);
    std::string printLiteral(const Literal& literal);
    std::string printUnary(const Unary& unary);    
};
}