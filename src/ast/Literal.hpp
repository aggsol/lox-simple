/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "BaseExpr.hpp"
#include "../ValueNext.hpp"

#include <iostream>

namespace bodhi
{
    class Literal : public BaseExpr
    {
    public:
        Literal()
        : BaseExpr(Type::Literal)
        {}

        explicit Literal(ValueNext& val)
        : BaseExpr(Type::Literal)
        , m_value(val)
        {
        }

        explicit Literal(const char* str)
        : Literal(std::string(str))
        {
        }

        explicit Literal(bool value)
        : BaseExpr(Type::Literal)
        , m_value(value)
        {
        }

        explicit Literal(double value)
        : BaseExpr(Type::Literal)
        , m_value(value)
        {
            // std::cout << "numeric literal " << value << "\n";
        }

        explicit Literal(const std::string& value)
        : BaseExpr(Type::Literal)
        , m_value(value)
        {
        }

        std::string typeName() const override { return "Literal"; }

        ~Literal() override {}

        const ValueNext& value() const { return m_value; }
        void value(ValueNext& val) { m_value = val; }

    private:
        Literal(const Literal&) = delete;
        Literal(Literal&&) = delete;

        ValueNext       m_value;
    };
}