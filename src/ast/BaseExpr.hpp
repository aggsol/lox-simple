/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#include <cassert>
#include <string>

#include "../Value.hpp"

namespace bodhi
{
    class Token;

    class BaseExpr
    {
    public:
        enum class Type
        {
            INVALID = 0,
            Assignment,
            Binary,
            Call,
            Get,
            Set,
            Unary,
            Logical,
            Literal,
            Grouping,
            Variable
        };

        virtual bool isVariable() const { return false; }
        virtual bool isGetExpr() const { return false; }

        virtual Token* getName() const { return nullptr; }
        virtual BaseExpr* getObject() const { return nullptr; }

        virtual std::string typeName() const { return "Base"; }

        unsigned id() const { return m_id; }
        Type type() const { return m_type; }

        virtual ~BaseExpr() {}

    protected:
        explicit BaseExpr(Type type)
        : m_id(0)
        , m_type(type)
        {
            static unsigned idSeed = 0;
            idSeed++;
            m_id = idSeed;
        }

    private:
        BaseExpr(const BaseExpr&) = delete;
        BaseExpr(BaseExpr&&) = delete;
        BaseExpr() = delete;

        unsigned m_id;
        Type m_type = Type::INVALID;
    };

    class Assignment : public BaseExpr
    {
    public:
        Assignment(Token& n, BaseExpr& val)
        : BaseExpr(Type::Assignment)
        , name(n)
        , value(val)
        {}
        Assignment(const Assignment&) = delete;
        Assignment(Assignment&&) = delete;

        ~Assignment() override {}

        Token* getName() const override { return &name; }

        std::string typeName() const override { return "Assignment"; }

        Token&      name;
        BaseExpr&   value;
    };

    class Binary : public BaseExpr
    {
    public:
        Binary(BaseExpr& lhs, Token& token, BaseExpr& rhs)
        : BaseExpr(Type::Binary)
        , left(lhs)
        , op(token)
        , right(rhs)
        {}
        Binary(const Binary&) = delete;
        Binary(Binary&&) = delete;

        ~Binary() override {}

        std::string typeName() const override { return "Binary"; }

        BaseExpr&       left;
        Token&          op;
        BaseExpr&       right;
    };

    class Grouping : public BaseExpr
    {
    public:

        explicit Grouping(BaseExpr& expr)
        : BaseExpr(Type::Grouping)
        , expression(expr)
        {}
        Grouping(const Grouping&) = delete;
        Grouping(Grouping&&) = delete;

        ~Grouping() override {}

        std::string typeName() const override { return "Grouping"; }

        BaseExpr&   expression;
    };

    class Unary : public BaseExpr
    {
    public:
        Unary(Token& token, BaseExpr& rhs)
        : BaseExpr(Type::Unary)
        , op(token)
        , right(rhs)
        {}

        Unary(const Unary&) = delete;
        Unary(Unary&&) = delete;

        ~Unary() override {}

        Token&      op;
        BaseExpr&   right;       
    };

}