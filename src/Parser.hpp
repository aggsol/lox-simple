/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_PARSER_HPP_
#define BODHI_PARSER_HPP_

#include "Statement.hpp"
#include "Expression.hpp"
#include "Token.hpp"

#include <deque>
#include <initializer_list>
#include <vector>

namespace bodhi
{
    class Logger;
    class Token;

    class Parser
    {
    public:
        Parser(std::vector<Token>& tokens, Logger& logger);

        std::deque<Stmnt::Base::Ptr>    parse();
        Expr::Base::Ptr                 parseExpression();

        bool                m_hasError = false;
    private:

        Expr::Base::Ptr     andExpr();
        Expr::Base::Ptr     assignment();
        Expr::Base::Ptr     callExpr();
        Expr::Base::Ptr     finishCall(Expr::Base::Ptr callee);
        Expr::Base::Ptr     expression();
        Expr::Base::Ptr     equality();
        Expr::Base::Ptr     comparison();
        Expr::Base::Ptr     term();
        Expr::Base::Ptr     factor();
        Expr::Base::Ptr     unary();
        Expr::Base::Ptr     orExpr();
        Expr::Base::Ptr     primary();

        std::deque<Stmnt::Base::Ptr>    block();
        Stmnt::ClassDecl::Ptr           classDeclaration();
        Stmnt::Base::Ptr                declaration();
        Stmnt::Function::Ptr            function(const std::string& kind);
        Stmnt::Base::Ptr                statement();
        Stmnt::Base::Ptr                print();
        Stmnt::Base::Ptr                expressionStmnt();
        Stmnt::Base::Ptr                varDeclaration();
        Stmnt::Base::Ptr                ifStmnt();
        Stmnt::Base::Ptr                whileStmnt();
        Stmnt::Base::Ptr                forStmnt();
        Stmnt::Base::Ptr                returnStatement();

        bool                    match(std::initializer_list<TokenType> types);
        bool                    check(TokenType type);
        bool                    isAtEnd();

        Token::Ptr              advance();
        Token::Ptr              peek();
        Token::Ptr              previous();
        Token::Ptr              consume(TokenType, const std::string&);

        void                    synchronize();

        std::vector<Token>& m_tokens;
        int                     m_current = 0;

        Logger&                 m_logger;
    };
}

#endif