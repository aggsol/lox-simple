/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_ENVIRONMENT_HPP
#define BODHI_ENVIRONMENT_HPP

#include "ICallable.hpp"
#include "Token.hpp"
#include "Value.hpp"

#include <memory>
#include <unordered_map>
#include <string>

namespace bodhi
{
    class Environment
    {
    public:
        typedef std::shared_ptr<Environment> Ptr;

        Environment();
        explicit Environment(Environment::Ptr other);

        unsigned depth() const { return m_depth; }

        void assignAt(int dist, Token::Ptr name, Value::Ptr val);
        void assign(Token::Ptr name, Value::Ptr val);
        void assign(Token::Ptr name, ICallable::Ptr callable);

        void define(const std::string& name);
        void define(const std::string& name, Value::Ptr val);
        void define(const std::string& name, int val);
        void define(const std::string& name, ICallable::Ptr callable);

        // TODO: Could this be const?
        Value::Ptr getAt(int dist, const std::string& name);
        Value::Ptr get(const std::string& name) const;
        Value::Ptr get(Token::Ptr name) const;

        Environment* ancestor(int dist);

    private:
        Environment(const Environment& other) = delete;

        std::unordered_map  <std::string, Value::Ptr>       m_values = {};
        std::unordered_map  <std::string, ICallable::Ptr>   m_callables = {};

        Environment::Ptr    m_enclosing;
        unsigned            m_depth = 0;
    };
}

#endif
