/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_LOX_FUNCTION_HPP
#define BODHI_LOX_FUNCTION_HPP

#include <memory>

#include "ICallable.hpp"
#include "Environment.hpp"

namespace bodhi
{
    namespace Stmnt
    {
        class Function;
    }

    class LoxFunction : public ICallable
    {
    public:
        typedef std::shared_ptr<LoxFunction> Ptr;

        LoxFunction(Stmnt::Function* decl, Environment::Ptr closure);
        LoxFunction() = delete;
        LoxFunction(const LoxFunction&) = delete;
        LoxFunction(LoxFunction&&) = delete;

        virtual unsigned arity() override;
        virtual Value::Ptr call(Interpreter& interpreter, const std::deque<Value::Ptr>& args) override;
        virtual std::string toString() const override;

        Stmnt::Function*    m_declaration;
        Environment::Ptr    m_closure;
    };
}

#endif
