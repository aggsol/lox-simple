/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_LOGGER_HPP
#define BODHI_LOGGER_HPP

#include "Token.hpp"

#include <string>
#include <iostream>

namespace bodhi
{
    class RuntimeError;

    class Logger
    {
    public:
        Logger();
        explicit Logger(std::ostream& strm);

        std::ostream&   log() { return m_output; }

        void debug(const std::string msg)
        {
#ifndef NDEBUG
            m_output << msg << std::endl;
#endif
        }

        bool    m_hadError = false;

        void    reportError(int line, const std::string& msg, const std::string& where);
        void    reportTokenError(Token token, const std::string& msg);
        void    reportRuntimeError(const RuntimeError& ex);

    private:

        std::ostream&   m_output;
    };
}

#endif
