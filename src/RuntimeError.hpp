/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_RUNTIME_ERROR_HPP
#define BODHI_RUNTIME_ERROR_HPP

#include "Token.hpp"

#include <stdexcept>

namespace bodhi
{
    class RuntimeError : public std::runtime_error
    {
    public:
        explicit RuntimeError(const std::string& msg)
        : std::runtime_error(msg)
        {}           

        RuntimeError(const std::string& msg, Token token)
        : std::runtime_error(msg)
        , m_token(token)
        {}

        Token m_token;
    };
}

#endif
