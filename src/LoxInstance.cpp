/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of lox-simple.

 lox-simple is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <sstream>

#include "LoxClass.hpp"
#include "LoxInstance.hpp"
#include "RuntimeError.hpp"

namespace bodhi
{
    std::string LoxInstance::toString() const
    {
        return m_klass->m_name + " instance";
    }

    Value::Ptr LoxInstance::get(Token::Ptr token)
    {
        auto it = m_fields.find(token->lexeme());
        if(it != m_fields.end())
        {
            return it->second;
        }

        auto method = m_klass->findMethod(this, token->lexeme());
        if(method != nullptr)
        {
            return method;
        }

        std::ostringstream msg;
        msg << "Undefined property '" << token->lexeme() << "'";
        throw RuntimeError(msg.str(), *token);

        return Value::getNilPtr();
    }

    void LoxInstance::set(Token::Ptr name, Value::Ptr value)
    {
        m_fields[name->lexeme()] = value;
    }

    std::ostream& operator<<(std::ostream& os, const LoxInstance& li)
    {
        os << li.toString();
        return os;
    }
}