/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_INTERPRETER_HPP
#define BODHI_INTERPRETER_HPP

#include "Expression.hpp"
#include "Environment.hpp"
#include "Statement.hpp"
#include "Token.hpp"
#include "Value.hpp"

#include <deque>
#include <unordered_map>

namespace test
{
    class InterpreterTest;
}

namespace bodhi
{
    class Logger;

    class Interpreter : public Expr::IVisitor, public Stmnt::IVisitor
    {
    public:

        explicit Interpreter(Logger& logger);

        ~Interpreter() override
        {}

        Environment::Ptr    globals() { return m_globals; }
        Environment::Ptr    environment() { return m_environment; }

        const std::string&  lastPrint() const { return m_lastPrint; }
        bool hasError() const { return m_hasError; }

        void interpret(std::deque<Stmnt::Base::Ptr>& statements);
        Value::Ptr interpret(Expr::Base& expr);
        void resolve(Expr::Base* expr, int depth);

        // expressions
        Value::Ptr visitAssignment(Expr::Assignment& expr) override;
        Value::Ptr visitBinary(Expr::Binary& expr) override;
        Value::Ptr visitCallExpr(Expr::Call& expr) override;
        Value::Ptr visitGet(Expr::Get& expr) override;
        Value::Ptr visitSet(Expr::Set& expr) override;
        Value::Ptr visitUnary(Expr::Unary& expr) override;
        Value::Ptr visitLogical(Expr::Logical& expr) override;
        Value::Ptr visitLiteral(Expr::Literal& expr) override;
        Value::Ptr visitGrouping(Expr::Grouping& expr) override;
        Value::Ptr visitVariable(Expr::Variable& expr) override;

        // statements
        void visitBlock(Stmnt::Block& stmnt) override;
        void visitClassDecl(Stmnt::ClassDecl& stmnt) override;
        void visitFunction(Stmnt::Function* stmnt) override;
        void visitExpression(Stmnt::Expression& stmnt) override;
        void visitIf(Stmnt::If& stmnt) override;
        void visitPrint(Stmnt::Print& stmnt) override;
        void visitReturn(Stmnt::Return& stmnt) override;
        void visitVar(Stmnt::Var& stmnt) override;
        void visitWhile(Stmnt::While& stmnt) override;

        void executeBlock(std::deque<Stmnt::Base::Ptr>& statements, Environment::Ptr env);
    private:

        void execute(Stmnt::Base& stmt);
        Value::Ptr  evaluate(Expr::Base& expr);
        void checkNumberOperand(Token::Ptr token, const Value& val);
        void checkNumberOperands(Token::Ptr token, const Value& left, const Value& right);
        Value::Ptr lookupVariable(Token& name, Expr::Base& expr);

        Logger&             m_logger;
        std::string         m_lastPrint;
        bool                m_hasError = false;

        std::unordered_map<unsigned, int>   m_locals;
        Environment::Ptr                    m_globals;
        Environment::Ptr                    m_environment;

    };
}

#endif
