/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_VALUE_HPP
#define BODHI_VALUE_HPP

#include <memory>
#include <string>

namespace bodhi
{
    class ICallable;
    class LoxInstance;

    /*
    * The book uses the Object type of Java for the different types. This class
    * is a helper to represent nil, bool, number, callable and instances.
    * TODO: think about move and copy semantics some more.
    */
    class Value
    {
    public:
        typedef std::shared_ptr<Value> Ptr;

        static Value getNil()
        {
            static Value nil;
            return nil;
        }

        static Value::Ptr getNilPtr()
        {

            static Value::Ptr nilPtr = std::make_shared<Value>(getNil());
            return nilPtr;
        }

        Value();
        explicit Value(const char* str);
        explicit Value(const std::string& value);
        explicit Value(std::shared_ptr<ICallable> callable);
        explicit Value(std::shared_ptr<LoxInstance> instance);

        explicit Value(int value);
        explicit Value(bool value);
        explicit Value(double value);

        Value(const Value& val)            = default;
        Value(Value&& val)                 = default;
        Value& operator=(const Value& val) = default;
        Value& operator=(Value&& val)      = default;

        void assign(const Value& other);
        void assign(double val);
        void assign(bool val);
        void assign(const std::string& val);
        void assign(const char* val);

        double                          getNumber() const noexcept;
        bool                            getBool() const noexcept;
        const std::string&              getString() const noexcept;
        ICallable*                      getCallable() const noexcept;
        std::shared_ptr<LoxInstance>    getInstance() const noexcept;

        bool operator==(const Value& other) const;
        bool operator!=(const Value& other) const;

        bool isNil() const noexcept { return m_tag == Tag::Nil; }
        bool isString() const noexcept;
        bool isNumber() const noexcept;
        bool isBool() const noexcept;
        // isTrue() works like isTruthy() in the book
        bool isTrue() const noexcept;
        bool isCallable() const;
        bool isInstance() const;

    private:

        enum class Tag
        {
            Nil,
            True,
            False,
            Number,
            String,
            Callable,
            Instance
        };

        Tag                             m_tag = Tag::Nil;
        std::string                     m_string;
        double                          m_number;
        std::shared_ptr<ICallable>      m_callable;
        std::shared_ptr<LoxInstance>    m_instance;

        friend std::ostream& operator<<(std::ostream& os, const Value& val);
    };

    std::ostream& operator<<(std::ostream& os, const Value& val);
}

#endif
