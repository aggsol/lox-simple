/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <iomanip>
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <limits>

#include "LoxInstance.hpp"
#include "Value.hpp"
#include "ICallable.hpp"

namespace bodhi
{
    Value::Value()
    : m_tag(Tag::Nil)
    , m_string("<nil>")
    , m_number(std::numeric_limits<double>::quiet_NaN())
    , m_callable(nullptr)
    , m_instance(nullptr)
    {}

    Value::Value(const char* str)
    : Value(std::string(str))
    {}

    Value::Value(const std::string& value)
    : m_tag(Tag::String)
    , m_string(value)
    , m_callable(nullptr)
    , m_instance(nullptr)
    {}

    Value::Value(std::shared_ptr<ICallable> callable)
    : m_tag(Tag::Callable)
    , m_callable(callable)
    , m_instance(nullptr)
    {
        assert(m_callable != nullptr);
        m_string = m_callable->toString();
    }


    Value::Value(std::shared_ptr<LoxInstance> instance)
    : m_tag(Tag::Instance)
    , m_callable(nullptr)
    , m_instance(instance)
    {
        assert(m_instance != nullptr);
        m_string = m_instance->toString();
    }

    Value::Value(bool value)
    {
        if(value)
        {
            m_tag = Tag::True;
        }
        else
        {
            m_tag = Tag::False;
        }
    }

    Value::Value(int i)
    : Value(static_cast<double>(i))
    {}

    Value::Value(double value)
    : m_tag(Tag::Number)
    , m_number(value)
    { }

    void Value::assign(const Value& other)
    {
        m_tag = other.m_tag;
        m_string = other.m_string;
        m_number = other.m_number;
        m_callable = other.m_callable;
        m_instance = other.m_instance;
    }

    void Value::assign(double val)
    {
        m_tag = Tag::Number;
        m_number = val;
        m_callable = nullptr;
        m_instance = nullptr;
        m_string = "";
    }

    void Value::assign(bool val)
    {
        if(val)
        {
            m_tag = Tag::True;
        }
        else
        {
            m_tag = Tag::False;
        }
        m_number = std::numeric_limits<double>::quiet_NaN();
        m_callable = nullptr;
        m_instance = nullptr;
        m_string = "";
    }

    void Value::assign(const std::string& val)
    {
        m_tag = Tag::String;
        m_string = val;
        m_number = std::numeric_limits<double>::quiet_NaN();
        m_callable = nullptr;
        m_instance = nullptr;
    }

    void Value::assign(const char* val)
    {
        m_tag = Tag::String;
        m_string = val;
        m_number = std::numeric_limits<double>::quiet_NaN();
        m_callable = nullptr;
        m_instance = nullptr;
    }

    bool Value::isString() const noexcept
    {
        return m_tag == Tag::String;
    }

    bool Value::isNumber() const noexcept
    {
        return m_tag == Tag::Number;
    }

    bool Value::isBool() const noexcept
    {
        return (m_tag == Tag::True) || (m_tag == Tag::False);
    }

    bool Value::isCallable() const
    {
        return (m_tag == Tag::Callable);
    }

    bool Value::isInstance() const
    {
        return (m_tag == Tag::Instance);
    }

    ICallable* Value::getCallable() const noexcept
    {
        assert(m_tag == Tag::Callable);
        return m_callable.get();
    }

    std::shared_ptr<LoxInstance> Value::getInstance() const noexcept
    {
        assert(m_instance != nullptr);
        return m_instance;
    }

    bool Value::operator==(const Value& other) const
    {
        if(this->isNil() && other.isNil()) return true;
        if(this->isNil()) return false;
        if(this->m_tag != other.m_tag) return false;

        const bool isLeftNum = this->isNumber();
        const bool isRightNum = other.isNumber();
        if(isLeftNum && isRightNum)
        {
            return this->m_number == other.m_number;
        }

        const bool isLeftString = this->isString();
        const bool isRightString = other.isString();
        if(isLeftString && isRightString)
        {
            return this->m_string == other.m_string;
        }

        if(this->isBool() && other.isBool())
        {
            return this->m_tag == other.m_tag;
        }

        return false;
    }

    bool Value::isTrue() const noexcept
    {
        if(m_tag == Tag::Nil) return false;
        if(m_tag == Tag::False) return false;

        return true;
    }

    bool Value::operator!=(const Value& other) const
    {
        return !(*this == other);
    }

    double Value::getNumber() const noexcept
    {
        assert(isNumber());
        return m_number;
    }

    bool Value::getBool() const noexcept
    {
        assert(isBool());
        return m_tag == Tag::True;
    }
    const std::string&  Value::getString() const noexcept
    {
        assert(isString());
        return m_string;
    }

    std::ostream& operator<<(std::ostream& os, const Value& val)
    {
        switch(val.m_tag)
        {
        case Value::Tag::Nil:
            os << "nil";
        break;
        case Value::Tag::Number:
            os << std::setprecision(std::numeric_limits<double>::digits10) << val.m_number;
        break;
        case Value::Tag::String:
            os << val.m_string;
        break;
        case Value::Tag::True:
            os << "true";
        break;
        case Value::Tag::False:
            os << "false";
        break;
        case Value::Tag::Callable:
            os << val.m_callable->toString();
        break;
        case Value::Tag::Instance:
            assert(val.m_instance);
            os << val.m_instance->toString();
        break;
        default:
            throw std::runtime_error("Invalid value tag");
        break;
        }

        // os << "@0x" << std::setfill('0') << std::setw(8) << std::hex << &val;

        return os;
    }
}