/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "LoxFunction.hpp"

#include "Environment.hpp"
#include "Interpreter.hpp"
#include "ReturnValue.hpp"

#include <cassert>

namespace bodhi
{
    LoxFunction::LoxFunction(Stmnt::Function* decl, Environment::Ptr closure)
    : m_declaration(decl)
    , m_closure(closure)
    {
        assert(closure != nullptr);
        assert(decl != nullptr);
    }

    Value::Ptr LoxFunction::call(Interpreter& interpreter, const std::deque<Value::Ptr>& args)
    {
        assert(m_declaration->m_parameters.size() == args.size());

        auto env = std::make_shared<Environment>(m_closure);
        for(unsigned i=0; i<m_declaration->m_parameters.size(); ++i)
        {
            env->define(m_declaration->m_parameters.at(i)->lexeme(), args.at(i));
        }

        try
        {
            interpreter.executeBlock(m_declaration->m_body, env);
        }
        catch(const ReturnValue& val)
        {
            // std::cout << "Got return value = '" << (*val.m_value) << "'\n";
            // interpreter.m_value = val.m_value;
            return val.m_value;
        }
        return std::make_shared<Value>(); // void
    }

    unsigned LoxFunction::arity()
    {
        return m_declaration->m_parameters.size();
    }

    std::string LoxFunction::toString() const
    {
        return "<fn " + m_declaration->m_name->lexeme() + ">";
    }
}
