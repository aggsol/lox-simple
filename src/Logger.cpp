/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Logger.hpp"

#include "RuntimeError.hpp"
#include "Token.hpp"
#include "TokenType.hpp"

#include <cassert>
#include <iostream>
#include <string>

namespace bodhi
{
    Logger::Logger()
    : Logger(std::cout)
    {}

    Logger::Logger(std::ostream& strm)
    : m_output(strm)
    {}

    void Logger::reportError(int line, const std::string& msg, const std::string& where)
    {
        m_output << "[line " << line << "] Error: " << where << ": " << msg << "\n";
        m_hadError = true;
    }

    void Logger::reportTokenError(Token token, const std::string& msg)
    {
        if(token.type() == TokenType::EndOfFile)
        {
            reportError(token.line(), " at end", msg);
        }
        else
        {
            reportError(token.line(), " at '" + token.lexeme() + "'", msg);
        }
    }

    void Logger::reportRuntimeError(const RuntimeError& ex)
    {
        if(ex.m_token.type() != TokenType::INVALID)
        {
            m_output << "[line " << ex.m_token.line() << "] Error: " << ex.what() << "\n";
        }
        else
        {
            m_output << "Error: " << ex.what() << "\n";
        }
        m_hadError = true;
    }
}