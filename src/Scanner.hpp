/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_SCANNER_HPP
#define BODHI_SCANNER_HPP

#include "Token.hpp"

#include <string>
#include <vector>

namespace test
{
    class ScannerTest;
}

namespace bodhi
{
    class Logger;

    class Scanner
    {
    public:
        friend class test::ScannerTest;

        explicit Scanner(const std::string& source, Logger& logger);

        std::vector<Token>&& scanTokens();
        bool    hasError() const {return m_hasError;}

    private:

        void    addToken(TokenType type);
        void    addToken(TokenType type, const std::string& literal);
        void    scanToken();

        char    advance();
        bool    isAtEnd() const;
        bool    match(char c);
        char    peek() const;
        char    peekNext() const;
        void    handleString();
        bool    isDigit(char c) const;
        bool    isAlpha(char c) const;
        bool    isAlphaNumeric(char c) const { return isDigit(c) || isAlpha(c); }
        void    handleNumber();
        void    handleIdentifier();
        size_t  numParsedChars() const { return m_current - m_start; }

        std::string                 m_source;
        std::vector<Token>     m_tokens;

        unsigned         m_start = 0;
        size_t      m_current = 0;
        unsigned    m_line = 0;
        Logger&     m_logger;
        bool        m_hasError = false;
    };
}

#endif
