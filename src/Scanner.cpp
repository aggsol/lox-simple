/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Scanner.hpp"

#include "Logger.hpp"
#include "TokenType.hpp"

#include <iostream>
#include <map>

namespace
{
    std::map<std::string, bodhi::TokenType> s_tokenMap =
    {
        {"and",     bodhi::TokenType::AND},
        {"class",   bodhi::TokenType::CLASS},
        {"else",    bodhi::TokenType::ELSE},
        {"false",   bodhi::TokenType::FALSE},
        {"for",     bodhi::TokenType::FOR},
        {"fun",     bodhi::TokenType::FUN},
        {"if",      bodhi::TokenType::IF},
        {"nil",     bodhi::TokenType::NIL},
        {"or",      bodhi::TokenType::OR},
        {"print",   bodhi::TokenType::PRINT},
        {"return",  bodhi::TokenType::RETURN},
        {"super",   bodhi::TokenType::SUPER},
        {"this",    bodhi::TokenType::THIS},
        {"true",    bodhi::TokenType::TRUE},
        {"var",     bodhi::TokenType::VAR},
        {"while",   bodhi::TokenType::WHILE},
        {"break",   bodhi::TokenType::BREAK},
    };
}

namespace bodhi
{
    Scanner::Scanner(const std::string& source, Logger& logger)
    : m_source(source)
    , m_tokens()
    , m_logger(logger)
    {}

    char Scanner::advance()
    {
        auto curr = m_source.at(m_current);
        m_current++;
        return curr;
    }

    void Scanner::addToken(TokenType type, const std::string& literal)
    {
        auto part = m_source.substr(m_start, numParsedChars());
        m_tokens.emplace_back(type, part, literal, m_line);
    }

    void Scanner::addToken(TokenType type)
    {
        addToken(type, "");
    }

    bool Scanner::isAtEnd() const
    {
        auto result = m_current >= m_source.size();
        return result;
    }

    bool Scanner::match(char c)
    {
        if(isAtEnd()) return false;
        if(m_source.at(m_current) != c) return false;

        m_current++;
        return true;
    }

    char Scanner::peek() const
    {
        if(isAtEnd()) return 0;
        return m_source.at(m_current);
    }

    char Scanner::peekNext() const
    {
        if (m_current + 1 >= m_source.size())
        {
            return '\0';
        }
        return m_source.at(m_current + 1);
    }

    void Scanner::handleString()
    {
        while(peek() != '"' && !isAtEnd())
        {
            if(peek() == '\n') m_line++;
            advance();
        }

        if(isAtEnd())
        {
            m_hasError = true;
            m_logger.reportError(m_line, "Unterminated string.", "");
            return;
        }

        // The closing ".
        advance();
        std::string value = m_source.substr(m_start + 1, numParsedChars() - 2);
        addToken(TokenType::STRING, value);
    }

    bool Scanner::isDigit(char c) const
    {
        return c >= '0' && c <= '9';
    }

    bool Scanner::isAlpha(char c) const
    {
        return (c >= 'a' && c <= 'z')
            || (c >= 'A' && c <= 'Z')
            || (c == '_');
    }

    void Scanner::handleNumber()
    {
        while(isDigit(peek()))
        {
            advance();
        }

        if(peek() == '.' && isDigit(peekNext()))
        {
            advance();
            while(isDigit(peek()))
            {
                advance();
            }
        }

        std::string value = m_source.substr(m_start, numParsedChars());
        addToken(TokenType::NUMBER, value);
    }

    void Scanner::handleIdentifier()
    {
        while(isAlphaNumeric(peek()))
        {
            advance();
        }

        auto text = m_source.substr(m_start, numParsedChars());

        //std::cout << "identifier text:" << text << ":\n";

        auto result = s_tokenMap.find(text);
        TokenType type = TokenType::IDENTIFIER;
        if(result != s_tokenMap.end()) {
            type = result->second;
        }

        addToken(type);
    }

    void Scanner::scanToken()
    {
        char c = advance();
        //std::cout << "next char: " << c << "\n";
        switch (c)
        {
            // single char lexemes
            case '(': addToken(TokenType::LEFT_PAREN); break;
            case ')': addToken(TokenType::RIGHT_PAREN); break;
            case '{': addToken(TokenType::LEFT_BRACE); break;
            case '}': addToken(TokenType::RIGHT_BRACE); break;
            case ',': addToken(TokenType::COMMA); break;
            case '.': addToken(TokenType::DOT); break;
            case ';': addToken(TokenType::SEMICOLON); break;
            case '*': addToken(TokenType::STAR); break;

            // two char lexemes
            case '!': addToken(match('=') ? TokenType::BANG_EQUAL : TokenType::BANG); break;
            case '=': addToken(match('=') ? TokenType::EQUAL_EQUAL : TokenType::EQUAL); break;
            case '<': addToken(match('=') ? TokenType::LESS_EQUAL : TokenType::LESS); break;
            case '>': addToken(match('=') ? TokenType::GREATER_EQUAL : TokenType::GREATER); break;
            case '+': addToken(match('+') ? TokenType::PLUS_PLUS : TokenType::PLUS); break;
            case '-': addToken(match('-') ? TokenType::MINUS_MINUS : TokenType::MINUS); break;

            // hashbang
            case '#':
                if(m_line != 1)
                {
                    m_hasError = true;
                    m_logger.reportError(m_line, "Unexpected character: #", "");
                }
                else if(match('!'))
                {
                    // hashbang is just a line
                    while (peek() != '\n' && !isAtEnd()) advance();
                }
                else
                {
                    m_hasError = true;
                    m_logger.reportError(m_line, "Invalid hashbang/shebang", "");
                }
            break;
            // comments
            case '/':
                if (match('/'))
                {
                    // A comment goes until the end of the line.
                    while (peek() != '\n' && !isAtEnd()) advance();
                }
                else if(match('*'))
                {
                    int nesting = 1;
                    while(nesting != 0 && !isAtEnd())
                    {
                        char last = advance();
                        if(last == '*' && match('/'))
                        {
                            nesting--;
                        }
                        else if(last == '/' && match('*'))
                        {
                            nesting++;
                        }
                        else if(last == '\n')
                        {
                            m_line++;
                        }

                    }
                    if(nesting > 0)
                    {
                        m_hasError = true;
                        m_logger.reportError(m_line, "Unexpected end of comment block", "");
                    }
                }
                else
                {
                  addToken(TokenType::SLASH);
                }
            break;

            case ' ':
            case '\r':
            case '\t':
                // Ignore whitespace.
            break;

            case '\n':
                m_line++;
            break;

            case '"':
                handleString();
            break;

            default:
                if(isDigit(c))
                {
                    handleNumber();
                }
                else if(isAlpha(c))
                {
                    handleIdentifier();
                }
                else
                {
                    m_hasError = true;
                    m_logger.reportError(m_line, std::string("Unexpected character: ") + c, "");
                }

            break;
        }
    }

    std::vector<Token>&& Scanner::scanTokens()
    {
        m_line = 1;
        m_current = 0;
        m_tokens.clear();

        while(! isAtEnd())
        {
            m_start = m_current;
            scanToken();
        }

        m_tokens.emplace_back(TokenType::EndOfFile, "", "", m_line);

        return std::move(m_tokens);
    }
}