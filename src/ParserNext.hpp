/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "Parser.hpp"
#include "ast/BaseExpr.hpp"

#include <vector>

namespace bodhi {

class Logger;
class Tree;

class ParserNext
{
public:
    ParserNext(Tree& tree, std::vector<Token>& tokens, Logger& logger);

    BaseExpr*   parseExpression();
private:

    BaseExpr*     andExpr();
    BaseExpr*     assignment();
    BaseExpr*     callExpr();
    BaseExpr*     finishCall(BaseExpr* callee);
    BaseExpr*     expression();
    BaseExpr*     equality();
    BaseExpr*     comparison();
    BaseExpr*     term();
    BaseExpr*     factor();
    BaseExpr*     unary();
    BaseExpr*     orExpr();
    BaseExpr*     primary();


    bool match(const std::initializer_list<TokenType>& types);
    bool check(TokenType type);
    bool isAtEnd();

    Token& advance();
    Token& peek();
    Token& previous();
    Token& consume(TokenType, const std::string&);
    void synchronize();
    
    std::vector<Token>& m_tokens;
    Logger&             m_logger;
    Tree&               m_tree;
    int                 m_current = 0;

};
}