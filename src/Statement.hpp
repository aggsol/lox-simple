/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_STATEMENT_HPP
#define BODHI_STATEMENT_HPP

#include "Expression.hpp"
#include "Statement.hpp"
#include "Token.hpp"

#include <deque>
#include <iostream>
#include <memory>


namespace bodhi
{
namespace Stmnt
{
    class Block;
    class ClassDecl;
    class Function;
    class Expression;
    class If;
    class Print;
    class Return;
    class Var;
    class While;

    class IVisitor
    {
    public:
        virtual void visitBlock(Block& stmnt) = 0;
        virtual void visitClassDecl(ClassDecl& stmnt) = 0;
        virtual void visitFunction(Function* stmnt) = 0;
        virtual void visitExpression(Expression& stmnt) = 0;
        virtual void visitIf(If& stmnt) = 0;
        virtual void visitPrint(Print& stmnt) = 0;
        virtual void visitReturn(Return& stmnt) = 0;
        virtual void visitVar(Var& stmnt) = 0;
        virtual void visitWhile(While& stmnt) = 0;

    protected:
        virtual ~IVisitor() {}
    };

    class Base
    {
    public:
        typedef std::shared_ptr<Base> Ptr;

        virtual void accept(IVisitor& visitor) = 0;

        virtual std::string name() const { return "Base"; }
        virtual Expr::Base::Ptr getExpr() const {return nullptr;}

    protected:
        virtual ~Base() {}
    private:
    };

    class If : public Base
    {
    public:
        If(Expr::Base::Ptr condition, Base::Ptr thenBranch, Base::Ptr elseBranch)
        : m_condition(condition)
        , m_thenBranch(thenBranch)
        , m_elseBranch(elseBranch)
        {}

        void accept(IVisitor& visitor) override
        {
            visitor.visitIf(*this);
        }

        std::string name() const override { return "If"; }

        Expr::Base::Ptr     m_condition;
        Base::Ptr           m_thenBranch;
        Base::Ptr           m_elseBranch;
    };

    class Var : public Base
    {
    public:
        Var(Token::Ptr name, Expr::Base::Ptr initializer)
        : m_name(name)
        , m_initializer(initializer)
        {}

        void accept(IVisitor& visitor) override
        {
            visitor.visitVar(*this);
        }

        std::string name() const override { return "Var"; }

        Token::Ptr      m_name;
        Expr::Base::Ptr m_initializer;
    };

    class While : public Base
    {
    public:
        While(Expr::Base::Ptr condition, Base::Ptr body)
        : m_condition(condition)
        , m_body(body)
        {}

        void accept(IVisitor& visitor) override
        {
            visitor.visitWhile(*this);
        }

        std::string name() const override { return "While"; }

        Expr::Base::Ptr m_condition;
        Base::Ptr       m_body;
    };

    class Return : public Base
    {
    public:
        explicit Return(Token::Ptr keyword, Expr::Base::Ptr value)
        : m_keyword(keyword)
        , m_value(value)
        {}

        virtual std::string name() const { return "Return"; }

        void accept(IVisitor& visitor) override
        {
            visitor.visitReturn(*this);
        }

        Token::Ptr      m_keyword;
        Expr::Base::Ptr m_value;
    };

    class Print : public Base
    {
    public:
        explicit Print(Expr::Base::Ptr expr)
        : m_expr(expr)
        {}

        virtual std::string name() const { return "Print"; }

        void accept(IVisitor& visitor) override
        {
            visitor.visitPrint(*this);
        }

        Expr::Base::Ptr m_expr;
    };

    class Block : public Base
    {
    public:
        explicit Block(const std::deque<Base::Ptr>& statements)
        : m_statements(statements)
        {}

        void accept(IVisitor& visitor) override
        {
            visitor.visitBlock(*this);
        }

        std::string name() const override { return "Block"; }

        std::deque<Base::Ptr>     m_statements;
    };

    class Expression : public Base
    {
    public:
        explicit Expression(Expr::Base::Ptr expr)
        : m_expr(expr)
        {}

        void accept(IVisitor& visitor) override
        {
            visitor.visitExpression(*this);
        }

        std::string name() const override { return "Expression"; }
        Expr::Base::Ptr getExpr() const override { return m_expr; }

        Expr::Base::Ptr m_expr;
    };

    class Function : public Base
    {
    public:
        typedef std::shared_ptr<Function> Ptr;

        virtual std::string name() const { return "Function"; }

        Function(Token::Ptr name, const std::deque<Token::Ptr>& params,
            const std::deque<Base::Ptr>& body)
        : m_name(name)
        , m_parameters(params)
        , m_body(body)
        {}

        virtual void accept(IVisitor& visitor)
        {
            visitor.visitFunction(this);
        }

        Token::Ptr                  m_name;
        std::deque<Token::Ptr>      m_parameters;
        std::deque<Base::Ptr>       m_body;
    };

    class ClassDecl : public Base
    {
    public:
        typedef std::shared_ptr<ClassDecl> Ptr;

        ClassDecl(Token::Ptr name, Expr::Base::Ptr super, const std::deque<Function::Ptr>& methods)
        : m_name(name)
        , m_superclass(super)
        , m_methods(methods)
        {}

        virtual std::string name() const { return "ClassDecl"; }

        void accept(IVisitor& visitor) override
        {
            visitor.visitClassDecl(*this);
        }

        Token::Ptr              m_name;
        Expr::Base::Ptr         m_superclass;
        std::deque<Function::Ptr>    m_methods;
    };
}
}
#endif
