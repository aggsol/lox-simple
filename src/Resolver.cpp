/*
 Copyright (C) 2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cassert>

#include "Interpreter.hpp"
#include "Logger.hpp"
#include "Resolver.hpp"

namespace bodhi
{
    Resolver::Resolver(Interpreter& interpreter, Logger& logger)
    : m_interpreter(interpreter)
    , m_logger(logger)
    {}

    void Resolver::beginScope()
    {
        std::unordered_map<std::string, bool> scope;
        m_scopes.push_back(scope);
    }

    void Resolver::endScope()
    {
        m_scopes.pop_back();
    }

     void Resolver::resolve(std::deque<Stmnt::Base::Ptr>& statements)
     {
         for(auto s: statements)
         {
             resolve(*s);
         }
     }

    // resolve statement
    void Resolver::resolve(Stmnt::Base& stmnt)
    {
        stmnt.accept(*this);
    }

    // resolve expression
    void Resolver::resolve(Expr::Base & expr)
    {
        expr.accept(*this);
    }

     void Resolver::resolveLocal(Expr::Base* expr, Token::Ptr name)
     {
        assert(name != nullptr);

        for(int i=m_scopes.size()-1; i>=0; --i)
        {
            auto& scope = m_scopes.at(i);
            auto it = scope.find(name->lexeme());
            if(it != scope.end())
            {
                int x = m_scopes.size() - 1 - i;
                m_interpreter.resolve(expr, x);
            }
        }
     }

    void Resolver::resolveFunction(Stmnt::Function& func, FunctionType ft)
    {
        auto enclosingFt = m_currentFunction;
        m_currentFunction = ft;

        beginScope();
        for(auto p: func.m_parameters)
        {
            declare(p);
            define(*p);
        }
        resolve(func.m_body);
        endScope();
        m_currentFunction = enclosingFt;
    }

    void Resolver::declare(Token::Ptr token)
    {
        if(m_scopes.size() == 0) return;

        auto& scope = m_scopes.back();
        auto it = scope.find(token->lexeme());
        if(it != scope.end())
        {
            m_logger.reportTokenError(*token, "Variable with this name already declared in this scope");
        }

        scope[token->lexeme()] = false;
    }

    void Resolver::define(Token& token)
    {
        if(m_scopes.size() == 0) return;

        auto& scope = m_scopes.back();
        scope.at(token.lexeme()) = true;
    }

    // expressions
    Value::Ptr Resolver::visitAssignment(Expr::Assignment& expr)
    {
        resolve(*expr.m_value);
        resolveLocal(&expr, expr.m_name);
        return nullptr;
    }

    Value::Ptr Resolver::visitBinary(Expr::Binary& expr)
    {
        resolve(*expr.left());
        resolve(*expr.right());
        return nullptr;
    }

    Value::Ptr Resolver::visitCallExpr(Expr::Call& expr)
    {
        resolve(*expr.m_callee);
        for(auto a: expr.m_arguments)
        {
            resolve(*a);
        }
        return nullptr;
    }

    Value::Ptr Resolver::visitUnary(Expr::Unary& expr)
    {
        resolve(*expr.m_right);
        return nullptr;
    }

    Value::Ptr Resolver::visitLogical(Expr::Logical& expr)
    {
        resolve(*expr.m_left);
        resolve(*expr.m_right);
        return nullptr;
    }

    Value::Ptr Resolver::visitLiteral(Expr::Literal& lit)
    {
        return lit.value();
    }

    Value::Ptr Resolver::visitGet(Expr::Get& expr)
    {
        resolve(*expr.m_object);
        return nullptr;
    }

    Value::Ptr Resolver::visitSet(Expr::Set& expr)
    {
        resolve(*expr.m_value);
        resolve(*expr.m_object);
        return nullptr;
    }

    Value::Ptr Resolver::visitGrouping(Expr::Grouping& expr)
    {
        resolve(*expr.m_expression);
        return nullptr;
    }

    Value::Ptr Resolver::visitVariable(Expr::Variable& expr)
    {
        if(m_scopes.size() != 0)
        {
            auto& currScope = m_scopes.back();
            auto it = currScope.find(expr.m_name->lexeme());
            if (it != currScope.end() && it->second == false) {
                m_logger.reportTokenError(*expr.m_name, "Cannot read local variable in its own initializer");
            }
        }

        resolveLocal(&expr, expr.m_name);
        return nullptr;
    }

    // statements
    void Resolver::visitBlock(Stmnt::Block& stmnt)
    {
        beginScope();
        resolve(stmnt.m_statements);
        endScope();
    }

    void Resolver::visitClassDecl(Stmnt::ClassDecl& stmnt)
    {
        declare(stmnt.m_name);
        define(*stmnt.m_name);
        for(auto method : stmnt.m_methods)
        {
            auto type = FunctionType::Method;
            resolveFunction(*method, type);
        }
    }

    void Resolver::visitFunction(Stmnt::Function* stmnt)
    {
        assert(stmnt != nullptr);
        declare(stmnt->m_name);
        define(*stmnt->m_name);
        resolveFunction(*stmnt, FunctionType::Function);
    }

    void Resolver::visitExpression(Stmnt::Expression& stmnt)
    {
        resolve(*stmnt.m_expr);
    }

    void Resolver::visitIf(Stmnt::If& stmnt)
    {
        resolve(*stmnt.m_condition);
        resolve(*stmnt.m_thenBranch);
        if(stmnt.m_elseBranch != nullptr)
        {
            resolve(*stmnt.m_elseBranch);
        }
    }

    void Resolver::visitPrint(Stmnt::Print& stmnt)
    {
        resolve(*stmnt.m_expr);
    }

    void Resolver::visitReturn(Stmnt::Return& stmnt)
    {
        if(m_currentFunction == FunctionType::None)
        {
            m_logger.reportTokenError(*stmnt.m_keyword, "Cannot return from top level code");
        }
        if(stmnt.m_value != nullptr)
        {
            resolve(*stmnt.m_value);
        }
    }

    void Resolver::visitVar(Stmnt::Var& stmnt)
    {
        assert(stmnt.m_name != nullptr);

        declare(stmnt.m_name);
        if(stmnt.m_initializer != nullptr)
        {
            resolve(*stmnt.m_initializer);
        }
        define(*stmnt.m_name);
    }

    void Resolver::visitWhile(Stmnt::While& stmnt)
    {
        resolve(*stmnt.m_condition);
        resolve(*stmnt.m_body);
    }

}