/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of lox-simple.

 lox-simple is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "LoxClass.hpp"
#include "LoxInstance.hpp"

#include <memory>

namespace bodhi
{

    unsigned LoxClass::arity()
    {
        return 0;
    }

    Value::Ptr LoxClass::call(Interpreter& /*interpreter*/, 
        const std::deque<Value::Ptr>& /*args*/)
    {
        auto instance = std::make_shared<LoxInstance>(this);
        return std::make_shared<Value>(instance);
    }

    std::string LoxClass::toString() const
    {
        return m_name;
    }

    Value::Ptr LoxClass::findMethod(LoxInstance* /*instance*/, 
        const std::string& name) const
    {
        auto it = m_methods.find(name);
        if(it != m_methods.end())
        {
            return it->second;
        }
        return nullptr;
    }

    std::ostream& operator<<(std::ostream& os, const LoxClass& lc)
    {
        os << lc.m_name;
        return os;
    }
}