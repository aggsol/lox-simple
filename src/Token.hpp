/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_TOKEN_HPP
#define BODHI_TOKEN_HPP

#include "TokenType.hpp"

#include <memory>
#include <string>

namespace bodhi
{

    class Token
    {
    public:
        typedef Token* Ptr;
        Token() = default;
        Token(const Token&) = default;
        Token(Token&&) = default;

        Token(TokenType type, const std::string& lexeme, const std::string& literal, int line);
    
        TokenType           type() const { return m_type; } 
        const std::string&  lexeme() const  { return m_lexeme; }
        const std::string&  literal() const { return m_literal; }
        int                 line() const { return m_line; }
    
    private:
        TokenType   m_type = TokenType::INVALID;
        std::string m_lexeme = "<empty lexeme>";
        std::string m_literal = "<empty literall>";
        int         m_line = -1;
    };
}

#endif
