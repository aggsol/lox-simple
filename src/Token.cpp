#include "Token.hpp"

#include <iostream>
#include <sstream>

namespace bodhi
{
    Token::Token(TokenType type, const std::string& lexeme, const std::string& literal, int line)
    : m_type(type)
    , m_lexeme(lexeme)
    , m_literal(literal)
    , m_line(line)
    {
    }
}