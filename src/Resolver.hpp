/*
 Copyright (C) 2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_RESOLVER_HPP_
#define BODHI_RESOLVER_HPP_

#include <unordered_map>
#include <stack>
#include <vector>

#include "Expression.hpp"
// #include "Environment.hpp"
#include "Statement.hpp"

namespace bodhi
{
    class Interpreter;
    class Logger;

    class Resolver : public Expr::IVisitor, public Stmnt::IVisitor
    {
    public:
        explicit Resolver(Interpreter& interpreter, Logger& logger);

         void resolve(std::deque<Stmnt::Base::Ptr>& statements);

        // expressions
        Value::Ptr visitAssignment(Expr::Assignment& expr) override;
        Value::Ptr visitBinary(Expr::Binary& expr) override;
        Value::Ptr visitCallExpr(Expr::Call& expr) override;
        Value::Ptr visitUnary(Expr::Unary& expr) override;
        Value::Ptr visitGet(Expr::Get& expr) override;
        Value::Ptr visitSet(Expr::Set& expr) override;
        Value::Ptr visitLogical(Expr::Logical& expr) override;
        Value::Ptr visitLiteral(Expr::Literal& expr) override;
        Value::Ptr visitGrouping(Expr::Grouping& expr) override;
        Value::Ptr visitVariable(Expr::Variable& expr) override;

        // statements
        void visitBlock(Stmnt::Block& stmnt) override;
        void visitClassDecl(Stmnt::ClassDecl& stmnt) override;
        void visitFunction(Stmnt::Function* stmnt) override;
        void visitExpression(Stmnt::Expression& stmnt) override;
        void visitIf(Stmnt::If& stmnt) override;
        void visitPrint(Stmnt::Print& stmnt) override;
        void visitReturn(Stmnt::Return& stmnt) override;
        void visitVar(Stmnt::Var& stmnt) override;
        void visitWhile(Stmnt::While& stmnt) override;

    private:
        enum class FunctionType
        {
            None,
            Function,
            Method
        };

        void resolve(Stmnt::Base& stmnt);
        void resolve(Expr::Base& expr);
        void resolveLocal(Expr::Base* expr, Token::Ptr name);
        void resolveFunction(Stmnt::Function& func, FunctionType ft);

        void beginScope();
        void endScope();
        void declare(Token::Ptr token);
        void define(Token& token);

        Interpreter&    m_interpreter;
        Logger&         m_logger;

        std::vector< std::unordered_map<std::string, bool> > m_scopes;
        FunctionType    m_currentFunction = FunctionType::None;
    };
}

#endif
