/*
    Copyright (C) 2020 Kim HOANG <foss@aggsol.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "ParserNext.hpp"
#include "Expression.hpp"
#include "TokenType.hpp"
#include "ast/BaseExpr.hpp"
#include "ast/Tree.hpp"
#include "ast/Literal.hpp"
#include "RuntimeError.hpp"
#include "Logger.hpp"
#include <cstdint>

namespace bodhi 
{
    ParserNext::ParserNext(Tree& tree, std::vector<Token>& tokens, Logger& logger)
    : m_tokens(tokens)
    , m_logger(logger)
    , m_tree(tree)
    {

    }

    BaseExpr* ParserNext::parseExpression()
    {
        try
        {
            return expression();
        } catch (const RuntimeError& error) 
        {
            m_logger.reportRuntimeError(error);
            return nullptr;
        }
    }

    BaseExpr* ParserNext::andExpr()
    {
        BaseExpr* expr = equality();
        while(match({TokenType::AND}))
        {
            Token& op = previous();
            BaseExpr* right = equality();
            // expr = m_tree.createExpr<Logical>(*expr, op, *right);
        }
        return expr;
    }

    BaseExpr* ParserNext::assignment()
    {
        BaseExpr* expr = orExpr();

        if(match({TokenType::EQUAL})) {
            Token& equals = previous();
            BaseExpr* valueExpr = assignment();
            assert(valueExpr != nullptr);
            if(expr->isVariable())
            {
                auto name = expr->getName();
                assert(name != nullptr);

                // return m_tree.createExpr<Assignment>(name, valueExpr);
            }
            else if(expr->isGetExpr())
            {
                // return m_tree.createExpr<Set>(expr->getObject(), expr->getName(), valueExpr);
            }

            throw RuntimeError("Invalid assginment target.", equals);
        }

        return expr;
    }

    BaseExpr* ParserNext::expression()
    {
        return equality();
    }

    BaseExpr* ParserNext::equality()
    {
        BaseExpr* expr = comparison();

        const auto types = {TokenType::BANG_EQUAL, TokenType::EQUAL_EQUAL};
        while(match(types))
        {
            Token& op = previous();
            BaseExpr* right = comparison();
            expr = m_tree.createExpr<Binary>(*expr, op, *right);
        }

        return expr;        
    }

    BaseExpr* ParserNext::comparison()
    {
        BaseExpr* expr = term();

        const auto types = {TokenType::GREATER, TokenType::GREATER_EQUAL,
            TokenType::LESS, TokenType::LESS_EQUAL };

        while(match(types))
        {
            Token& op = previous();
            auto right = term();
            expr = m_tree.createExpr<Binary>(*expr, op, *right);
        }

        return expr;
    }

    BaseExpr* ParserNext::term()
    {
        BaseExpr* expr = factor();

        const auto types = {TokenType::MINUS, TokenType::PLUS};
        while(match(types))
        {
            Token& op = previous();
            auto right = factor();
            expr = m_tree.createExpr<Binary>(*expr, op, *right);
        }

        return expr;
    }

    BaseExpr* ParserNext::factor()
    {
        BaseExpr* expr = unary();
        const auto types = {TokenType::SLASH, TokenType::STAR};

        while(match(types))
        {
            Token& op = previous();
            auto right = unary();
            expr = m_tree.createExpr<Binary>(*expr, op, *right);
        }

        return expr;
    }

    BaseExpr* ParserNext::unary()
    {
        const auto tokens = {TokenType::MINUS, TokenType::BANG};

        if(match(tokens))
        {
            Token& op = previous();
            auto right = unary();
            return m_tree.createExpr<Unary>(op, *right);
        }

        return callExpr();
    }

    BaseExpr* ParserNext::orExpr()
    {
        auto expr = andExpr();

        while(match({TokenType::OR}))
        {
            Token& op = previous();
            auto right = andExpr();
            // expr = m_tree.createExpr<Logical>(expr, op, right);
        }

        return expr;
    }

    BaseExpr* ParserNext::callExpr()
    {
        BaseExpr* expr = primary();
        while(true)
        {
            if(match({TokenType::LEFT_PAREN}))
            {
                expr = finishCall(expr);
            }
            else if(match({TokenType::DOT}))
            {
                auto name = consume(TokenType::IDENTIFIER, "Expect property name after '.'");
                // expr = m_tree.createExpr<Get>(expr, name);
            }
            else
            {
                break;
            }
        }
        return expr;
    }

    BaseExpr* ParserNext::finishCall(BaseExpr* callee)
    {
        std::deque<BaseExpr*> arguments;
        if(not check(TokenType::RIGHT_PAREN))
        {
            do
            {
                if(arguments.size() >= 8) {
                    m_logger.reportTokenError(peek(), "Cannot have more than 8 arguments.");
                    //throw RuntimeError("Cannot have more than 8 arguments.", peek());
                }
                arguments.push_back(expression());
            }
            while(match({TokenType::COMMA}));
        }

        auto paren = consume(TokenType::RIGHT_PAREN, "Expect ')' after arguments.");

        // TODO:
        // return m_tree.createExpr<Call>(callee, paren, arguments);
        return nullptr;
    }

    BaseExpr* ParserNext::primary()
    {
        if(match({TokenType::FALSE}))
        {
            return m_tree.createExpr<Literal>(false);
        }
        if(match({TokenType::TRUE}))
        {
            return m_tree.createExpr<Literal>(true);
        }
        if(match({TokenType::NIL}))
        {
            return m_tree.createExpr<Literal>();
        }
        if(match({TokenType::STRING}))
        {
            auto lit = m_tree.createExpr<Literal>(previous().literal());
            //std::cout << "ParserNext::primary() Literal string: '" << *lit->value() << "'\n";
            return lit;
        }
        if(match({TokenType::NUMBER}))
        {
            double value = std::stod(previous().literal(), nullptr);
            return m_tree.createExpr<Literal>(value);
        }
        if(match({TokenType::IDENTIFIER}))
        {
            // TODO: 
            // auto var =  m_tree.createExpr<Variable>(previous());
            //std::cout << "ParserNext::primary() Literal string: '" << var->m_name->lexeme() << "'\n";
            //return var;
        }
        if(match({TokenType::LEFT_PAREN}))
        {
            BaseExpr* expr = expression();
            consume(TokenType::RIGHT_PAREN, "Expect ')' after expression. ");
            return m_tree.createExpr<Grouping>(*expr);
        }

        const auto errors = {TokenType::PLUS, 
            TokenType::BANG_EQUAL, TokenType::EQUAL_EQUAL,
            TokenType::GREATER, TokenType::GREATER, TokenType::LESS, TokenType::LESS_EQUAL,
            TokenType::SLASH, 
            TokenType::STAR};
        if(match(errors))
        {
            throw RuntimeError("Missing left-hand operand.", previous());
        }

        m_logger.reportTokenError(peek(), "Expect expression");
        throw RuntimeError("Expect expression", peek());
    }

    // --- internal state progress ---

    bool ParserNext::match(const std::initializer_list<TokenType>& types)
    {
        for(auto& t: types)
        {
            if(check(t))
            {
                advance();
                return true;
            }
        }

        return false;
    }

    bool ParserNext::check(TokenType type)
    {
        if(isAtEnd())
        {
            return false;
        }
        return peek().type() == type;
    }

    bool ParserNext::isAtEnd()
    {
        return peek().type() == TokenType::EndOfFile;
    }

    Token& ParserNext::advance()
    {
        if(not isAtEnd())
        {
            m_current++;
        }
        return previous();
    }

    Token& ParserNext::peek()
    {
        return m_tokens.at(m_current);
    }

    Token& ParserNext::previous()
    {
        return m_tokens.at(m_current-1);
    }

    Token& ParserNext::consume(TokenType type, const std::string& msg)
    {
        if(check(type))
        {
            return advance();
        }
        throw RuntimeError(msg, peek());
    }

    void ParserNext::synchronize()
    {
        advance();
        while(not isAtEnd())
        {
            if(previous().type() == TokenType::SEMICOLON) return;

            switch(peek().type())
            {
                case TokenType::CLASS:
                case TokenType::FUN:
                case TokenType::VAR:
                case TokenType::FOR:
                case TokenType::IF:
                case TokenType::WHILE:
                case TokenType::PRINT:
                case TokenType::RETURN:
                return;

                default:
                break;
            }

            advance();
        }
    }
}