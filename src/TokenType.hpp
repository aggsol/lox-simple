/*
 Copyright (C) 2017  Kim HOANG <foss@aggsol.de>
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_TOKEN_TYPE_HPP
#define BODHI_TOKEN_TYPE_HPP

#include <string>
#include <iostream>

namespace bodhi
{
    enum class TokenType
    {
        INVALID = 0,
        // Single-character tokens.
        LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE,
        COMMA, DOT, SEMICOLON, SLASH, STAR,

        // One or two character tokens.
        BANG, BANG_EQUAL,
        EQUAL, EQUAL_EQUAL,
        GREATER, GREATER_EQUAL,
        LESS, LESS_EQUAL,
        MINUS, PLUS,
        MINUS_MINUS, PLUS_PLUS,

        // Literals.
        IDENTIFIER, STRING, NUMBER,

        // Keywords.
        AND, CLASS, ELSE, FALSE, FUN, FOR, IF, NIL, OR,
        PRINT, RETURN, SUPER,
        THIS,
        TRUE,
        VAR,
        WHILE,
        BREAK,
        EndOfFile
    };

    std::string convertToString(TokenType type);

    std::ostream& operator<<(std::ostream& os, const TokenType& fr);
}

#endif
