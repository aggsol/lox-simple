/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cassert>
#include <sstream>

#include "ClockFunction.hpp"
#include "ICallable.hpp"
#include "Interpreter.hpp"
#include "Logger.hpp"
#include "LoxClass.hpp"
#include "LoxFunction.hpp"
#include "LoxInstance.hpp"
#include "ReturnValue.hpp"
#include "RuntimeError.hpp"
#include "TokenType.hpp"

namespace bodhi
{
    Interpreter::Interpreter(Logger& logger)
    : m_logger(logger)
    , m_lastPrint("<empty>")
    {
        m_environment = std::make_shared<Environment>();
        m_globals = m_environment;
        auto fun = std::make_shared<ClockFunction>();
        m_globals->define("clock", std::make_shared<Value>(fun));
    }

    Value::Ptr Interpreter::visitAssignment(Expr::Assignment& expr)
    {
        assert(expr.m_value != nullptr);

        auto val = evaluate(*expr.m_value);

        auto it = m_locals.find(expr.id());
        if(it != m_locals.end())
        {
            m_environment->assignAt(it->second, expr.m_name, val);
        }
        else
        {
            m_globals->assign(expr.m_name, val);
        }
        m_environment->assign(expr.m_name, val);
        //std::cout << "assigned " << expr.m_name->lexeme() << "=" << *val << "\n";
        return val;
    }

    Value::Ptr Interpreter::visitBinary(Expr::Binary& expr)
    {
        auto op = expr.op()->type();


        auto left = evaluate(*expr.left());

        auto right = evaluate(*expr.right());

        const bool isLeftString = left->isString();
        const bool isRightString = right->isString();

        switch(op)
        {
            case TokenType::MINUS:
                checkNumberOperands(expr.op(), *left, *right);
                return std::make_shared<Value>(left->getNumber() - right->getNumber());
            break;
            case TokenType::PLUS:

                if(left->isNumber() && right->isNumber())
                {
                    return std::make_shared<Value>(left->getNumber() + right->getNumber());
                }
                else if(isLeftString || isRightString)
                {
                    std::ostringstream both;
                    both << *left << *right;
                    return std::make_shared<Value>(both.str());
                }
                else
                {
                    throw RuntimeError("Operands for (+) must be two numbers or string.", 
                        *expr.op());
                }
            break;
            case TokenType::SLASH:
                checkNumberOperands(expr.op(), *left, *right);
                if(right->getNumber() == 0)
                {
                    throw RuntimeError("Division by zero.", *expr.op());
                }
                return std::make_shared<Value>(left->getNumber() / right->getNumber());
            break;
            case TokenType::STAR:
                checkNumberOperands(expr.op(), *left, *right);
                return std::make_shared<Value>(left->getNumber() * right->getNumber());
            break;
            case TokenType::GREATER:
                checkNumberOperands(expr.op(), *left, *right);
                return std::make_shared<Value>(left->getNumber() > right->getNumber());
            break;
            case TokenType::GREATER_EQUAL:
                checkNumberOperands(expr.op(), *left, *right);
                return std::make_shared<Value>(left->getNumber() >= right->getNumber());
            break;
            case TokenType::LESS:
                checkNumberOperands(expr.op(), *left, *right);
                return std::make_shared<Value>(left->getNumber() < right->getNumber());
            break;
            case TokenType::LESS_EQUAL:
                checkNumberOperands(expr.op(), *left, *right);
                return std::make_shared<Value>(left->getNumber() <= right->getNumber());
            break;
            case TokenType::EQUAL_EQUAL:
            // std::cout << "EQUAL_EQUAL left=" << *left << " right=" << *right << "\n";
                return std::make_shared<Value>(*left == *right);
            break;
            case TokenType::BANG_EQUAL:
                return std::make_shared<Value>(*left != *right);
            break;
            default:
                    std::ostringstream msg;
                    msg << "Cannot " << op
                        << " left=" << left
                        << " right=" << right << "\n";
                    m_logger.reportTokenError(*expr.op(), msg.str());
            break;
        }
        return Value::getNilPtr();
    }

    Value::Ptr Interpreter::visitCallExpr(Expr::Call& expr)
    {
        auto callee = evaluate(*expr.m_callee);

        if(! callee->isCallable())
        {
            throw RuntimeError("Can only call functions and classes.", *expr.m_paren);
        }

        std::deque<Value::Ptr> args;
        for(auto a: expr.m_arguments)
        {
            auto val = evaluate(*a);
            args.push_back(val);
        }

        auto fun = callee->getCallable();
        assert(fun != nullptr);
        if(fun->arity() != args.size())
        {
            std::ostringstream msg;
            msg << "Expected " << fun->arity() << " arguments but got " << args.size() << ".";
            throw RuntimeError(msg.str(), *expr.m_paren);
        }

        return fun->call(*this, args);
    }

    Value::Ptr Interpreter::visitGet(Expr::Get& expr)
    {
        auto object = evaluate(*expr.m_object);
        if(object->isInstance())
        {
            return object->getInstance()->get(expr.m_name);
        }
        throw RuntimeError("Only instances have properties", *expr.m_name);
    }

    Value::Ptr Interpreter::visitSet(Expr::Set& expr)
    {
        auto object = evaluate(*expr.m_object);
        if(not object->isInstance())
        {
            throw RuntimeError("Only instances have fields", *expr.m_name);
        }
        auto value = evaluate(*expr.m_value);
        object->getInstance()->set(expr.m_name, value);
        return value;
    }

    Value::Ptr Interpreter::visitGrouping(Expr::Grouping& expr)
    {
        return evaluate(*expr.m_expression);
    }

    Value::Ptr Interpreter::visitLiteral(Expr::Literal& expr)
    {
        assert(expr.value() != nullptr);
        return expr.value();
    }

    Value::Ptr Interpreter::visitLogical(Expr::Logical& expr)
    {
        auto left = evaluate(*expr.m_left);
        if(expr.m_operator->type() == TokenType::OR)
        {
            if(left->isTrue()) return left;
        }
        else // AND
        {
            if(! left->isTrue()) return left;
        }

        return evaluate(*expr.m_right);
    }

    Value::Ptr Interpreter::visitUnary(Expr::Unary& expr)
    {
        auto right = evaluate(*expr.m_right);;

        if(expr.m_operator->type() == TokenType::MINUS)
        {
            checkNumberOperand(expr.m_operator, *right);
            return std::make_shared<Value>( -(right->getNumber()) );
        }
        else if(expr.m_operator->type() == TokenType::BANG)
        {
          return std::make_shared<Value>(! right->isTrue());
        }
        return Value::getNilPtr();
    }

    Value::Ptr Interpreter::lookupVariable(Token& name, Expr::Base& expr)
    {
        auto it = m_locals.find(expr.id());
        if(it != m_locals.end())
        {
            return m_environment->getAt(it->second, name.lexeme());
        }

        return m_globals->get(name.lexeme());
    }

    Value::Ptr Interpreter::visitVariable(Expr::Variable& expr)
    {
        return lookupVariable(*expr.m_name, expr);
    }

    Value::Ptr Interpreter::evaluate(Expr::Base& expr)
    {
        auto val = expr.accept(*this);
        if(val == nullptr) {
            std::cout << "Evaluated " << expr.typeName() << " to nullptr\n";
        }
        return val;
    }

    void Interpreter::execute(Stmnt::Base& stmnt)
    {
        stmnt.accept(*this);
    }

    void Interpreter::checkNumberOperand(Token::Ptr token, const Value& val)
    {
         if(val.isNumber()) return;
         throw RuntimeError("Operand must be a number.", *token);
    }

    void Interpreter::checkNumberOperands(Token::Ptr token, const Value& left,
        const Value& right)
    {
        // std::cout << "number operation: " << left << token->lexeme() << right << "\n";

        if(left.isNumber() && right.isNumber()) return;
        throw RuntimeError("Operands must be a numbers.", *token);
    }

    Value::Ptr Interpreter::interpret(Expr::Base& expr)
    {
        try
        {
            auto val = evaluate(expr);
            assert(val != nullptr);
            return val;
        }
        catch(const RuntimeError& ex)
        {
            std::cerr << "-> Interpreter::interpret exception\n";
            m_logger.reportRuntimeError(ex);
        }
        return Value::getNilPtr();
    }

    void Interpreter::interpret(std::deque<Stmnt::Base::Ptr>& statements)
    {
        try
        {
            for(auto stmnt : statements)
            {
                assert(stmnt != nullptr);
                execute(*stmnt);
            }
        }
        catch(const RuntimeError& ex)
        {
            m_logger.reportRuntimeError(ex);
            m_hasError = true;
            m_lastPrint = "<error>";
        }
    }

    void Interpreter::resolve(Expr::Base* expr, int depth)
    {
        assert(expr != nullptr);
        m_locals[expr->id()] = depth;
    }

    void Interpreter::executeBlock(std::deque<Stmnt::Base::Ptr>& statements, Environment::Ptr env)
    {
        assert(env);
        assert(statements.size() > 0);
        assert(env != m_environment);

        auto prev = m_environment;
        try
        {
            m_environment = env;

            for(unsigned i=0; i<statements.size(); ++i)
            {
                assert(statements[i] != nullptr);
                execute(*statements[i]);
            }
        }
        catch(...)
        {
            m_environment = prev;
            throw;
        }
        m_environment = prev;
    }

    void Interpreter::visitBlock(Stmnt::Block& stmnt)
    {
        executeBlock(stmnt.m_statements, std::make_shared<Environment>(m_environment));
        return;
    }

    void Interpreter::visitFunction(Stmnt::Function* stmnt)
    {
        auto function = std::make_shared<LoxFunction>(stmnt, m_environment);
        m_environment->define(stmnt->m_name->lexeme(), std::make_shared<Value>(function));
    }

    void Interpreter::visitExpression(Stmnt::Expression& stmt)
    {
        evaluate(*stmt.m_expr);
    }

    void Interpreter::visitPrint(Stmnt::Print& stmt)
    {
        auto val = evaluate(*stmt.m_expr);
        assert(val != nullptr);

        std::cout << *val << std::endl;
        std::ostringstream rep;
        rep << *val;
        m_lastPrint = rep.str();
    }

    void Interpreter::visitVar(Stmnt::Var& stmt)
    {
        Value::Ptr val;
        if(stmt.m_initializer != nullptr)
        {
            val =  evaluate(*stmt.m_initializer);
        }
        else
        {
            val = Value::getNilPtr();
        }
        assert(val != nullptr);
        assert(stmt.m_name != nullptr);

        m_environment->define(stmt.m_name->lexeme(), val);
    }

    void Interpreter::visitWhile(Stmnt::While& stmnt)
    {
        auto val = evaluate(*stmnt.m_condition);
        assert(val != nullptr);

        unsigned count = 0;
        while(val->isTrue())
        {
            execute(*stmnt.m_body);
            val = evaluate(*stmnt.m_condition);
            assert(val != nullptr);

            count++;
        }
    }

    void Interpreter::visitIf(Stmnt::If& stmnt)
    {
        auto val = evaluate(*stmnt.m_condition);
        assert(val != nullptr);

        if(val->isTrue())
        {
            execute(*stmnt.m_thenBranch);
        }
        else if(stmnt.m_elseBranch != nullptr)
        {
            execute(*stmnt.m_elseBranch);
        }
    }

    void Interpreter::visitReturn(Stmnt::Return& stmnt)
    {
        Value::Ptr value;
        if(stmnt.m_value != nullptr) {
            // std::cout << "eval Return\n";
            value = evaluate(*stmnt.m_value);
            // std::cout << "eval Return DONE\n";
        }
        else
        {
            value = Value::getNilPtr();
        }
        assert(value != nullptr);
        // std::cout << "Interpreter::visitReturn  Return value = '" << (*value) << "' EXIT" << std::endl;

        throw ReturnValue(value);
    }

    void Interpreter::visitClassDecl(Stmnt::ClassDecl& stmnt)
    {
        m_environment->define(stmnt.m_name->lexeme());

        std::unordered_map<std::string, Value::Ptr> methods;
        for(auto m : stmnt.m_methods)
        {
            // NOTE: This is weird, I would expect LocFunction to use m directly
            // but the visitor pattern with smart pointer this is even weirder.
            auto function = std::make_shared<LoxFunction>(m.get(), m_environment);
            auto value = std::make_shared<Value>(function);
            methods[m->m_name->lexeme()] = value;
        }

        auto klass = std::make_shared<LoxClass>(stmnt.m_name->lexeme(), methods);

        m_environment->assign(stmnt.m_name, klass);
    }
}
