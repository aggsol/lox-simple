/**
* Command Line Arguments
* Adhering the POSIX and GNU command line parameter standards.
*
* POSIX:
* Arguments are options if they begin with a hyphen delimiter (‘-’).
* Options are a hyphen followed by a single alphanumeric character: '-o'
* Options may require an argument: '-o argument' or '-oargument'
* Options without arguments are of type bool and true when set
* Options without arguments can be grouped after a hyphen: '-lst' same as '-t -l -s'
* Options can appear in any order: '-lst' same as '-tls'
* The '--' argument terminates options, following arguments are non-options
*
* GNU:
* Define long name options: '--output' for '-o'
* Always provide '-h', '--help' and '--version' options.
*
* Usage:
* 
*   bodhi::CliArguments args(argc, argv);
*    
*   auto name = args.getOpt<std::string>("n", "name", "none");
*   auto count = args.getOpt<int>("c", "count", -1);
*    
*   auto version = args.getOpt("", "version");
*   auto help = args.getOpt("h", "help");
*
* Version: 2.0.1
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* <foss@aggsol.de> wrote this file. As long as you retain this
* notice you can do whatever you want with this stuff. If we meet some day,
* and you think this stuff is worth it, you can buy me a beer in return KIM
* ----------------------------------------------------------------------------
*/
#ifndef BODHI_CLI_ARGUMENTS_HPP
#define BODHI_CLI_ARGUMENTS_HPP

#include <cassert>
#include <deque>
#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>

namespace bodhi
{
    class CliArguments
    {
    public:
        CliArguments(int argc, const char* const argv[])
        : m_arguments()
        , m_value()
        {
            if(argc < 1)
            {
                throw std::invalid_argument("Invalid argc.");
            }

            if(argv == nullptr)
            {
                throw std::invalid_argument("Invalid argv.");
            }
            // NOTE: skip the first, that is the program name
            for(int i=1; i<argc; ++i)
            {
                m_arguments.push_back(argv[i]);
            }
        }
    
        /*
        * Returns the remaining arguments
        */
        const std::deque<std::string>& arguments() const { return m_arguments; }
    
        /*
        * Returns the value of long or short options if found, fallback value otherwise
        */
        template <typename T>
        T   getOpt(const std::string& opt, const std::string& longOpt, const T& fallback )
        {
            static_assert(! std::is_same<T, bool>::value, "Use bool specialisation");
            
            if(opt.length() > 1)
            {
                throw std::invalid_argument("Short option must be one character or empty.");
            }

            if(longOpt.length() == 1)
            {
                throw std::invalid_argument("Long option must not be one character.");
            }
            
            if(m_arguments.size() == 0) return fallback;
            
            if(! search(opt))
            {
                if(! search(longOpt))
                {
                    if(! searchCondensed(opt))
                    {
                        return fallback;
                    }
                }
            }
            
            T value;
            std::istringstream(m_value) >> value;
            return value;
        }
    
        bool getOpt(const std::string& opt, const std::string& longOpt)
        {
            if(opt.length() > 1)
            {
                throw std::invalid_argument("Short option must be one character or empty.");
            }

            if(longOpt.length() == 1)
            {
                throw std::invalid_argument("Long option must not be one character.");
            }
            
            for(unsigned i=0; i<m_arguments.size(); ++i)
            {
                std::string& arg = m_arguments.at(i);
                if(arg.size() < 2) continue;
                
                // condensed bool options -xyz
                if(opt.length() == 1 && arg[0] == '-' && arg[1] != '-')
                {
                    auto pos = arg.find(opt);
                    if(pos != std::string::npos)
                    {
                        arg.erase(pos, 1);
                        if(arg == "-") // removed all bool options
                        {
                            m_arguments.erase(m_arguments.begin() + i);
                        }
                        return true;
                    }
                }
                else if(arg[0] == '-' && arg[1] == '-')
                {
                    // skip '--'
                    if(arg.length() == 2) return false;
                    
                    if(arg.substr(2) == longOpt)
                    {
                        m_arguments.erase( m_arguments.begin() + i);
                        return true;
                    }
                }

            }
            return false;
        }
    
    private:
    
        /**
        * Search for condensed options like -x123
        * Returns true if found, false otherwise
        */
        bool searchCondensed(const std::string& name)
        {
            if(name.empty()) return false;
            
            assert(name.size() == 1);
            
            for(unsigned i=0; i<(m_arguments.size()); ++i)
            {
                const std::string& arg = m_arguments.at(i);
                if(arg.size() < 3) continue;
                
                if(arg[0] == '-' && arg[1] != '-')
                {
                    if(arg.substr(1, 1) == name)
                    {
                        m_value = arg.substr(2);
                        m_arguments.erase(m_arguments.begin() + i);
                        return true;
                    }
                }
            }
            return false;
        }
    
        /**
        * Search for the named options and erase it and its value.
        * The value is copied to m_value.
        * Returns true when found, false otherwise.
        */
        bool search(const std::string& name)
        {
            if(name.empty()) return false;
            
            for(unsigned i=0; i<(m_arguments.size() - 1); ++i)
            {
                const std::string& arg = m_arguments.at(i);
                
                if(arg.size() < 2) continue;
                
                if(arg[0] == '-' && arg[1] != '-')
                {
                    if(arg.substr(1) == name)
                    {
                        m_value = m_arguments.at(i + 1);
                        // erase option and its value
                        m_arguments.erase( m_arguments.begin() + i, m_arguments.begin() + i + 2);
                        return true;
                    }
                }
                else if(arg[0] == '-' && arg[1] == '-')
                {
                    // skip '--'
                    if(arg.length() == 2) return false;
                    
                    if(arg.substr(2) == name)
                    {
                        m_value = m_arguments.at(i + 1);
                        
                        // erase option and its value
                        m_arguments.erase( m_arguments.begin() + i, m_arguments.begin() + i + 2);
                        return true;
                    }
                }

            }
            return false;
        }
    
        std::deque<std::string>     m_arguments;
        std::string                 m_value; 
    };
}

#endif
