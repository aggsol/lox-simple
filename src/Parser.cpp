/*
 Copyright (C) 2017-2018  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Parser.hpp"

#include "Logger.hpp"
#include "RuntimeError.hpp"
#include "Token.hpp"
#include "TokenType.hpp"

#include <cassert>
#include <deque>
#include <memory>
#include <stdexcept>

namespace bodhi
{
    Parser::Parser(std::vector<Token>& tokens, Logger& logger)
    : m_tokens(tokens)
    , m_logger(logger)
    {

    }

    Expr::Base::Ptr Parser::expression()
    {
        return assignment();
    }

    Stmnt::Base::Ptr Parser::statement()
    {
        if(match({TokenType::FOR}))
        {
            return forStmnt();
        }
        if(match({TokenType::IF}))
        {
            return ifStmnt();
        }
        if(match({TokenType::PRINT}))
        {
            return print();
        }
        if(match({TokenType::RETURN}))
        {
            return returnStatement();
        }
        if(match({TokenType::WHILE}))
        {
            return whileStmnt();
        }
        if(match({TokenType::LEFT_BRACE}))
        {
            return std::make_shared<Stmnt::Block>(block());
        }
        return expressionStmnt();
    }

    Expr::Base::Ptr Parser::andExpr()
    {
        auto expr = equality();
        while(match({TokenType::AND}))
        {
            auto op = previous();
            auto right = equality();
            expr = std::make_shared<Expr::Logical>(expr, op, right);
        }
        return expr;
    }

    Expr::Base::Ptr Parser::assignment()
    {
        auto expr = orExpr();

        if(match({TokenType::EQUAL})) {
            auto equals = previous();
            auto valueExpr = assignment();
            assert(valueExpr != nullptr);
            if(expr->isVariable())
            {
                auto name = expr->getName();
                assert(name != nullptr);

                return std::make_shared<Expr::Assignment>(name, valueExpr);
            }
            else if(expr->isGetExpr())
            {
                return std::make_shared<Expr::Set>(expr->getObject(), expr->getName(), valueExpr);
            }

            throw RuntimeError("Invalid assginment target.", *equals);
        }

        return expr;
    }

    Expr::Base::Ptr Parser::equality()
    {
        auto expr = comparison();

        auto types = {TokenType::BANG_EQUAL, TokenType::EQUAL_EQUAL};
        while(match(types))
        {
            Token::Ptr op = previous();
            Expr::Base::Ptr right = comparison();
            expr = std::make_shared<Expr::Binary>(expr, op, right);
        }

        return expr;
    }

    Expr::Base::Ptr Parser::comparison()
    {
        auto expr = term();

        auto types = {TokenType::GREATER, TokenType::GREATER_EQUAL,
            TokenType::LESS, TokenType::LESS_EQUAL };

        while(match(types))
        {
            auto op = previous();
            auto right = term();
            expr = std::make_shared<Expr::Binary>(expr, op, right);
        }

        return expr;
    }

    Expr::Base::Ptr Parser::term()
    {
        auto expr = factor();

        auto types = {TokenType::MINUS, TokenType::PLUS};
        while(match(types))
        {
            auto op = previous();
            auto right = factor();
            expr = std::make_shared<Expr::Binary>(expr, op, right);
        }

        return expr;
    }

    Expr::Base::Ptr Parser::factor()
    {
        auto expr = unary();
        auto types = {TokenType::SLASH, TokenType::STAR};

        while(match(types))
        {
            auto op = previous();
            auto right = unary();
            expr = std::make_shared<Expr::Binary>(expr, op, right);
        }

        return expr;
    }

    Expr::Base::Ptr Parser::unary()
    {
        auto types = {TokenType::MINUS, TokenType::BANG};

        if(match(types))
        {
            auto op = previous();
            auto right = unary();
            return std::make_shared<Expr::Unary>(op, right);
        }

        return callExpr();
    }

    Expr::Base::Ptr Parser::orExpr()
    {
        auto expr = andExpr();

        while(match({TokenType::OR}))
        {
            auto op = previous();
            auto right = andExpr();
            expr = std::make_shared<Expr::Logical>(expr, op, right);
        }

        return expr;
    }

    Expr::Base::Ptr Parser::callExpr()
    {
        auto expr = primary();
        while(true)
        {
            if(match({TokenType::LEFT_PAREN}))
            {
                expr = finishCall(expr);
            }
            else if(match({TokenType::DOT}))
            {
                auto name = consume(TokenType::IDENTIFIER, "Expect property name after '.'");
                expr = std::make_shared<Expr::Get>(expr, name);
            }
            else
            {
                break;
            }
        }
        return expr;
    }

    Expr::Base::Ptr Parser::finishCall(Expr::Base::Ptr callee)
    {
        std::deque<Expr::Base::Ptr> arguments;
        if(! check(TokenType::RIGHT_PAREN))
        {
            do
            {
                if(arguments.size() >= 8) {
                    m_logger.reportTokenError(*peek(), "Cannot have more than 8 arguments.");
                    //throw RuntimeError("Cannot have more than 8 arguments.", peek());
                }
                arguments.push_back(expression());
            }
            while(match({TokenType::COMMA}));
        }

        auto paren = consume(TokenType::RIGHT_PAREN, "Expect ')' after arguments.");

        return std::make_shared<Expr::Call>(callee, paren, arguments);
    }

    Expr::Base::Ptr Parser::primary()
    {

        if(match({TokenType::FALSE}))
        {
            return std::make_shared<Expr::Literal>(false);
        }
        if(match({TokenType::TRUE}))
        {
            return std::make_shared<Expr::Literal>(true);
        }
        if(match({TokenType::NIL}))
        {
            return std::make_shared<Expr::Literal>(Value::getNilPtr());
        }
        if(match({TokenType::STRING}))
        {
            auto lit = std::make_shared<Expr::Literal>(previous()->literal());
            //std::cout << "Parser::primary() Literal string: '" << *lit->value() << "'\n";
            return lit;
        }
        if(match({TokenType::NUMBER}))
        {
            double value = std::stod(previous()->literal(), nullptr);
            return std::make_shared<Expr::Literal>(value);
        }
        if(match({TokenType::IDENTIFIER}))
        {
            auto var =  std::make_shared<Expr::Variable>(previous());
            //std::cout << "Parser::primary() Literal string: '" << var->m_name->lexeme() << "'\n";
            return var;
        }
        if(match({TokenType::LEFT_PAREN}))
        {
            auto expr = expression();
            consume(TokenType::RIGHT_PAREN, "Expect ')' after expression. ");
            return std::make_shared<Expr::Grouping>(expr);
        }

        m_logger.reportTokenError(*peek(), "Expect expression");
        throw RuntimeError("Expect expression", *peek());
    }

    bool Parser::match(std::initializer_list<TokenType> types)
    {
        for(auto t: types)
        {
            if(check(t))
            {
                advance();
                return true;
            }
        }

        return false;
    }

    bool Parser::check(TokenType type)
    {
        if(isAtEnd())
        {
            return false;
        }
        return peek()->type() == type;
    }

    bool Parser::isAtEnd()
    {
        return peek()->type() == TokenType::EndOfFile;
    }

    Token::Ptr Parser::advance()
    {
        if(! isAtEnd())
        {
            m_current++;
        }
        return previous();
    }

    Token::Ptr Parser::peek()
    {
        return &m_tokens.at(m_current);
    }

    Token::Ptr Parser::previous()
    {
        return &m_tokens.at(m_current-1);
    }

    Token::Ptr Parser::consume(TokenType type, const std::string& msg)
    {
        if(check(type))
        {
            return advance();
        }
        throw RuntimeError(msg, *peek());
    }

    void Parser::synchronize()
    {
        advance();
        while(! isAtEnd())
        {
            if(previous()->type() == TokenType::SEMICOLON) return;

            switch(peek()->type())
            {
                case TokenType::CLASS:
                case TokenType::FUN:
                case TokenType::VAR:
                case TokenType::FOR:
                case TokenType::IF:
                case TokenType::WHILE:
                case TokenType::PRINT:
                case TokenType::RETURN:
                return;

                default:
                break;
            }

            advance();
        }
    }

    Expr::Base::Ptr Parser::parseExpression()
    {
        try {
            return expression();
        }
        catch(const RuntimeError& ex)
        {
            m_hasError = true;
            m_logger.reportRuntimeError(ex);
            return nullptr;
        }
    }

    std::deque<Stmnt::Base::Ptr> Parser::parse()
    {
        m_hasError = false;
        std::deque<Stmnt::Base::Ptr> statements;
        try
        {
            while(! isAtEnd())
            {
                auto decl = declaration();
                if (decl != nullptr)
                {
                    statements.push_back(decl);
                }
            }
        }
        catch(const RuntimeError& ex)
        {
            m_logger.reportRuntimeError(ex);
            m_hasError = true;
        }
        return statements;
    }

    std::deque<Stmnt::Base::Ptr> Parser::block()
    {
        std::deque<Stmnt::Base::Ptr> statements;

        while(! check(TokenType::RIGHT_BRACE) && !isAtEnd())
        {
            statements.push_back(declaration());
        }

        consume(TokenType::RIGHT_BRACE, "Expect '}' after block.");

        return statements;
    }



    Stmnt::Base::Ptr Parser::print()
    {
        auto value = expression();
        consume(TokenType::SEMICOLON, "Expect ';' after value.");
        auto stmt = std::make_shared<Stmnt::Print>(value);
        return stmt;
    }

    Stmnt::Base::Ptr Parser::expressionStmnt()
    {
        auto expr = expression();
        consume(TokenType::SEMICOLON, "Expect ';' after expression.");
        auto stmt = std::make_shared<Stmnt::Expression>(expr);
        return stmt;
    }

    Stmnt::Base::Ptr Parser::varDeclaration()
    {
        auto name = consume(TokenType::IDENTIFIER, "Expect variable name.");
        Expr::Base::Ptr init;
        if(match({TokenType::EQUAL}))
        {
            init = expression();
        }

        consume(TokenType::SEMICOLON, "Expect ';' after variable declaration.");
        return std::make_shared<Stmnt::Var>(name, init);
    }

    Stmnt::Base::Ptr Parser::declaration()
    {
        try
        {
            if (match({ TokenType::CLASS }))
            {
                return classDeclaration();
            }

            if(match({TokenType::FUN}))
            {
                return function("function");
            }

            if(match({TokenType::VAR}))
            {
                return varDeclaration();
            }
            return statement();
        }
        catch(const RuntimeError& ex)
        {
            m_hasError = true;
            synchronize();
            return nullptr;
        }
    }

    Stmnt::ClassDecl::Ptr Parser::classDeclaration()
    {
        auto name = consume(TokenType::IDENTIFIER, "Expect class name");
        consume(TokenType::LEFT_BRACE, "Expect '{' before class body");

        std::deque<Stmnt::Function::Ptr> methods;
        while (not check(TokenType::RIGHT_BRACE) && not isAtEnd())
        {
            auto func = function("method");
            methods.push_back(func);
        }

        consume(TokenType::RIGHT_BRACE, "Expect '}' after class body");

        return std::make_shared<Stmnt::ClassDecl>(name, nullptr, methods);
    }

    Stmnt::Function::Ptr Parser::function(const std::string& kind)
    {
        auto name = consume(TokenType::IDENTIFIER, "Expect " + kind + " name");

        // std::cout << "Parser function name = '" << name->lexeme() << "'\n";
        consume(TokenType::LEFT_PAREN, "Expect '(' after " + kind + " name");
        std::deque<Token::Ptr> params;
        if(! check(TokenType::RIGHT_PAREN))
        {
            do
            {
                if(params.size() >= 8)
                {
                    m_logger.reportTokenError(*peek(), "Cannot have more than 8 parameters");
                }
                auto paramName = consume(TokenType::IDENTIFIER, "Expect parameter name");
                // std::cout << "Parser " << params.size() << ". param name = '" << paramName->lexeme() << "'\n";
                params.push_back(paramName);
            }
            while(match({TokenType::COMMA}));
        }
        consume(TokenType::RIGHT_PAREN, "Expect ')' after parameters");
        consume(TokenType::LEFT_BRACE, "Expect '{' before " + kind + " body");
        std::deque<Stmnt::Base::Ptr> body = block();

        // std::cout << "Parser function body=" << body.size() << "\n";

        return std::make_shared<Stmnt::Function>(name, params, body);
    }

    Stmnt::Base::Ptr Parser::ifStmnt()
    {
        consume(TokenType::LEFT_PAREN, "Expect '(' after 'if'");
        auto condition = expression();
        consume(TokenType::RIGHT_PAREN, "Expect '(' after if condition");

        auto thenBranch = statement();
        Stmnt::Base::Ptr elseBranch = nullptr;

        if(match({TokenType::ELSE}))
        {
            elseBranch = statement();
        }

        return std::make_shared<Stmnt::If>(condition, thenBranch, elseBranch);
    }

    Stmnt::Base::Ptr Parser::whileStmnt()
    {
        consume(TokenType::LEFT_PAREN, "Expect '(' after 'while'.");
        auto condition = expression();
        consume(TokenType::RIGHT_PAREN, "Expect ')' after while condition.");

        auto body = statement();

        return std::make_shared<Stmnt::While>(condition, body);
    }

    Stmnt::Base::Ptr Parser::forStmnt()
    {
        consume(TokenType::LEFT_PAREN, "Expect '(' after 'for'.");

        Stmnt::Base::Ptr initializer;
        if(match({TokenType::SEMICOLON}))
        {
            initializer = nullptr;
        }
        else if(match({TokenType::VAR}))
        {
            initializer = varDeclaration();
        }
        else
        {
            initializer = expressionStmnt();
        }

        Expr::Base::Ptr condition;
        if(! check(TokenType::SEMICOLON))
        {
            condition = expression();
        }
        consume(TokenType::SEMICOLON, "Expect ';' after for loop condition.");

        Expr::Base::Ptr increment;
        if(! check(TokenType::RIGHT_PAREN))
        {
            increment = expression();
        }

        consume(TokenType::RIGHT_PAREN, "Expect ')' after for clauses.");
        auto body = statement();

        if(increment != nullptr)
        {
            std::deque<Stmnt::Base::Ptr> stmnts;
            stmnts.push_back( body );
            stmnts.push_back( std::make_shared<Stmnt::Expression>(increment) );

            body = std::make_shared<Stmnt::Block>(std::move(stmnts));
        }

        if(condition == nullptr)
        {
            condition = std::make_shared<Expr::Literal>(true);
        }

        body = std::make_shared<Stmnt::While>(condition, body);

        if(initializer != nullptr)
        {
            std::deque<Stmnt::Base::Ptr> stmnts;
            stmnts.push_back( initializer );
            stmnts.push_back( body );

            body = std::make_shared<Stmnt::Block>(std::move(stmnts));
        }

        return body;

    }

    Stmnt::Base::Ptr Parser::returnStatement()
    {
        auto keyword = previous();
        Expr::Base::Ptr expr;
        if(! check(TokenType::SEMICOLON)) {
            expr = expression();
            // std::cout << "Parser::returnStatement  Return statement with value.\n";
        }
        consume(TokenType::SEMICOLON, "Expect ';' after return value");
        return std::make_shared<Stmnt::Return>(keyword, expr);
    }
}