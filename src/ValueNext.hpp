/*
 Copyright (C) 2020  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <variant>
#include <string>

namespace bodhi
{

class ValueNext
{
    typedef std::variant<double, std::string> InnerValue;
public:
    ValueNext();
    explicit ValueNext(const char* str);
    explicit ValueNext(const std::string& value);

    explicit ValueNext(int value);
    explicit ValueNext(bool value);
    explicit ValueNext(double value);

    ValueNext(const ValueNext&)            = default;
    ValueNext(ValueNext&&)                 = default;
    ValueNext& operator=(const ValueNext&) = default;
    ValueNext& operator=(ValueNext&&)      = default;

    void assign(const ValueNext& other);
    void assign(double val);
    void assign(bool val);
    void assign(const std::string& val);
    void assign(const char* val);

    bool getBool() const;
    double getDouble() const;
    std::string getString() const;

    bool operator==(const ValueNext& other) const;
    bool operator!=(const ValueNext& other) const;

    bool isNil() const noexcept { return m_tag == Tag::Nil; }
    bool isString() const noexcept { return m_tag == Tag::String; };
    bool isNumber() const noexcept { return m_tag == Tag::Number; };
    bool isBool() const noexcept { return m_tag == Tag::True || m_tag == Tag::False; };
    bool isTruthy() const noexcept  { return m_tag != Tag::False && m_tag != Tag::Nil; };
    bool isCallable() const noexcept { return m_tag == Tag::Number; };
    bool isInstance() const { return m_tag == Tag::Number; };

    friend std::ostream& operator<<(std::ostream& os, const ValueNext& val);
private:
    enum class Tag
    {
        Nil = 0,
        True,
        False,
        Number,
        String,
        Callable,
        Instance
    };

    Tag m_tag = Tag::Nil;
    InnerValue      m_value;
};

std::ostream& operator<<(std::ostream& os, const ValueNext& val);

}