/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of lox-simple.

 lox-simple is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_LOX_INSTANCE_HPP
#define BODHI_LOX_INSTANCE_HPP

#include <cassert>
#include <string>
#include <memory>
#include <unordered_map>

#include "Token.hpp"
#include "Value.hpp"

namespace bodhi
{
    class LoxClass;
    class LoxInstance
    {
    public:
        typedef std::shared_ptr<LoxInstance> Ptr;

        explicit LoxInstance(LoxClass* klass)
        : m_klass(klass)
        {
            assert(klass != nullptr);
        }

        Value::Ptr get(Token::Ptr token);
        void set(Token::Ptr name, Value::Ptr value);

        std::string toString() const;

    private:
        std::unordered_map<std::string, Value::Ptr>   m_fields;

        LoxClass*   m_klass;
    };

    std::ostream& operator<<(std::ostream& os, const LoxInstance& li);
}

#endif