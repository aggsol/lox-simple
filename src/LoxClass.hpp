/*
 Copyright (C) 2018  Kim HOANG <foss@aggsol.de>
 This file is part of lox-simple.

 lox-simple is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_LOX_CLASS_HPP
#define BODHI_LOX_CLASS_HPP

#include <string>
#include <unordered_map>

#include "ICallable.hpp"

namespace bodhi
{
    class LoxClass : public ICallable
    {
    public:
        LoxClass(const std::string& name, const std::unordered_map<std::string, Value::Ptr>& methods)
        : m_name(name)
        , m_methods(methods)
        {
        }

        virtual unsigned arity() override;
        virtual Value::Ptr call(Interpreter& interpreter, const std::deque<Value::Ptr>& args) override;
        virtual std::string toString() const override;

        Value::Ptr findMethod(LoxInstance* instance, const std::string& name) const;

        std::string m_name;
        std::unordered_map<std::string, Value::Ptr> m_methods;
    };

    std::ostream& operator<<(std::ostream& os, const LoxClass& lc);
}


#endif